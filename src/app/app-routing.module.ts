import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {
  MonthlySummaryDataTableComponent
} from "./components/monthly-summary-data-table/monthly-summary-data-table.component";
import {
  AnnualSummaryDataTableComponent
} from "./components/annual-summary-data-table/annual-summary-data-table.component";

const routes: Routes = [
  {
    path: '',
    component: MonthlySummaryDataTableComponent
  },
  // {
  //   path: 'annual-summary',
  //   component: AnnualSummaryDataTableComponent
  // },
  // {
  //   path: '',
  //   redirectTo: 'monthly-summary',
  //   pathMatch: 'full'
  // }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
