import {Component} from '@angular/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html'
})
export class SidebarComponent {

  isMonthlySummarySectionActivated = true;

  changeActivatedSection(activeMonthlySummarySection: boolean): void {
    this.isMonthlySummarySectionActivated = activeMonthlySummarySection;
  }

}
