import {Injectable} from '@angular/core';
import {KJUR} from 'jsrsasign';
import {CLIENT_KEY, CLIENT_TOKEN, USER_TOKEN} from "../../app.constants";

@Injectable({
  providedIn: 'root'
})
export class TokenProviderService {

  private readonly header: any = {
    alg: 'HS256',
    typ: 'JWT'
  };
  private readonly sHeader!: string;

  constructor() {
    this.sHeader = JSON.stringify(this.header);
  }

  public generateToken(): string {
    const payload = {
      userToken: USER_TOKEN,
      clientToken: CLIENT_TOKEN,
      time: KJUR.jws.IntDate.get('now + 1hour'),
      mode: 'god'
    };
    const sPayload = JSON.stringify(payload);
    return KJUR.jws.JWS.sign('HS256', this.sHeader, sPayload, {utf8: CLIENT_KEY});
  }

}
