import {Injectable} from "@angular/core";
import {InvoiceService} from "../api/invoice.service";
import {OrderService} from "../api/order.service";
import {ProjectService} from "../api/project.service";
import {PurchaseService} from "../api/purchase.service";
import {TimesReportService} from "../api/times-reports.service";
import {DeliveryService} from "../api/delivery.service";
import {ResourceService} from "../api/resource.service";
import {NgxSpinnerService} from "ngx-spinner";
import {AlertService} from "../alert/alert.service";
import {Moment} from "moment";
import * as moment from "moment";
import {IMonthlySummaryDataTableData} from "../../models/data-table/monthly-summary-data-table-data.model";
import {forkJoin, Observable, of, ReplaySubject} from "rxjs";
import {IMonthlySummaryTotalsAmountsData} from "../../models/data-table/monthly-summary-totals-amounts-data.model";
import {IBoondManagerArrayResponse} from "../../models/api/boondmanager-array-response.model";
import {HttpResponse} from "@angular/common/http";
import {IBoondManagerResponse} from "../../models/api/boondmanager-response.model";
import {catchError, tap} from "rxjs/operators";
import {DataType} from "../../models/api/data-type.model";

type EntityResponseType = HttpResponse<IBoondManagerResponse>;
type EntityArrayResponseType = HttpResponse<IBoondManagerArrayResponse>;

@Injectable({
  providedIn: 'root'
})
export class MonthlySummaryDataBuilderService {

  startDate: Moment = moment().startOf('month');
  endDate: Moment = moment().add(1, 'month').endOf('month');

  private data: IMonthlySummaryDataTableData[] = [];
  private dataSubject = new ReplaySubject<{ totalsAmountsData: IMonthlySummaryTotalsAmountsData, monthlySummaryDataTableData: IMonthlySummaryDataTableData[] }>(1);

  resources!: IBoondManagerArrayResponse;


  constructor(
    private invoiceService: InvoiceService,
    private orderService: OrderService,
    private projectService: ProjectService,
    private purchaseService: PurchaseService,
    private timesReportService: TimesReportService,
    private deliveryService: DeliveryService,
    private resourceService: ResourceService,
    private spinner: NgxSpinnerService,
    private alertService: AlertService
  ) {
  }

  getData(): Observable<{ totalsAmountsData: IMonthlySummaryTotalsAmountsData, monthlySummaryDataTableData: IMonthlySummaryDataTableData[] }> {
    return this.dataSubject.asObservable();
  }

  private broadcastData(): void {
  }

  private handleHttpError(error: any): void {
    console.log(error);
    this.spinner.hide();
  }

  private handleObservableItemError(error: any): Observable<undefined> {
    console.log(error);
    return of(undefined);
  }

  private generateCommercialFullName(res: EntityResponseType): string | undefined {
    const commercialLastName: string | undefined = res.body?.included?.find(inc => inc.type === DataType.RESOURCE && inc.id === res.body?.data?.relationships?.mainManager?.data?.id)?.attributes?.lastName;
    const commercialFirstName: string | undefined = res.body?.included?.find(inc => inc.type === DataType.RESOURCE && inc.id === res.body?.data?.relationships?.mainManager?.data?.id)?.attributes?.firstName;
    if (commercialLastName && commercialFirstName) {
      return `${commercialLastName} ${commercialFirstName}`;
    } else if (commercialLastName) {
      return commercialLastName;
    } else if (commercialFirstName) {
      return commercialFirstName;
    } else {
      return undefined;
    }
  }

  buildDataTableData(): void {
    this.data = [];
    this.spinner.show();
    this.resourceService.getAllResources().subscribe(
      (response: HttpResponse<IBoondManagerArrayResponse>) => {
        this.resources = response.body!;
        if (this.resources.data?.length !== 0) {
          this.initializeData();
        } else {
          this.broadcastData();
        }
      },
      error => this.handleHttpError(error)
    );
  }

  private initializeData(): void {
    if (!this.resources.data) {
      return;
    }

    let array: Observable<EntityResponseType | undefined>[] = [];

    this.resources.data.forEach(item => {
      const itemObservable: Observable<EntityResponseType | undefined> = this.resourceService.getResourceInformation(item.id!)
        .pipe(
          catchError(err => this.handleObservableItemError(err)),
          tap((res: EntityResponseType | undefined) => {
            this.data.push({
              consultantID: res?.body?.data?.id!,
              consultantFullName: `${res?.body?.data?.attributes?.lastName} ${res?.body?.data?.attributes?.firstName}`,
              isEmployee: ((res?.body?.data?.attributes?.typeOf === 0) || (res?.body?.data?.attributes?.typeOf === 4 || res?.body?.data?.attributes?.state === 3)) ? true : res?.body?.data?.attributes?.typeOf === 1 ? false : undefined,
              commercialID: res?.body?.data?.relationships?.mainManager?.data?.id,
              commercialFullName: this.generateCommercialFullName(res!)
            });
          })
        );
      array.push(itemObservable);
    });


    forkJoin(array).subscribe(res => {

      let array2: Observable<EntityArrayResponseType | undefined>[] = [];

      this.data = this.data.filter(row => row.isEmployee !== undefined);
      this.data.forEach(item => {

        const itemObservable2: Observable<EntityArrayResponseType | undefined> = this.resourceService.getResourceDeliveriesInformation(item.consultantID!)
          .pipe(
            catchError(err => this.handleObservableItemError(err)),
            tap((res: EntityArrayResponseType | undefined) => this.checkResourceBenefits(item.consultantID!, res!))
          );

        array2.push(itemObservable2);

      });


    });


  }

  private checkResourceBenefits(resourceID: number, benifits: EntityArrayResponseType): void {
    // const indexItemToUpdate: number = this.data.findIndex(resource => resource.consultantID === resourceID);
    // if (indexItemToUpdate !== -1) {
    //   const itemToUpdate: IMonthlySummaryDataTableData = this.data[indexItemToUpdate];
    //   itemToUpdate.hasBenifits = benifits.body?.data?.length! > 0;
    //   itemToUpdate.hasBenifitsForThisMonth = benifits.body?.data?.filter(item => this.checkIfThisMonthIsIncludedInBenifitPeriod(moment(item.attributes?.startDate), moment(item.attributes?.endDate))).length! > 0;
    //   itemToUpdate.benifitIdForThisMonth = itemToUpdate.hasBenifitsForThisMonth ? benifits.body?.data?.find(item => this.checkIfThisMonthIsIncludedInBenifitPeriod(moment(item.attributes?.startDate), moment(item.attributes?.endDate)))?.id : undefined;
    //   itemToUpdate.projectIDForThisMonth = itemToUpdate.hasBenifitsForThisMonth ? benifits.body?.data?.find(item => this.month.isSame(moment(item.attributes?.startDate), 'month') || this.month.isSame(moment(item.attributes?.endDate), 'month'))?.relationships?.project?.data?.id : undefined
    //   this.data.splice(indexItemToUpdate, 1, itemToUpdate);
    // }
  }


}
