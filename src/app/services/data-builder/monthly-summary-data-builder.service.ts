import {Injectable} from '@angular/core';
import {IMonthlySummaryDataTableData} from "../../models/data-table/monthly-summary-data-table-data.model";
import {ResourceService} from "../api/resource.service";
import {HttpResponse} from "@angular/common/http";
import {IBoondManagerResponse} from "../../models/api/boondmanager-response.model";
import {IBoondManagerArrayResponse} from "../../models/api/boondmanager-array-response.model";
import {forkJoin, Observable, ReplaySubject} from "rxjs";
import {concatMap, tap} from "rxjs/operators";
import {DataType} from "../../models/api/data-type.model";
import * as moment from 'moment';
import {Moment} from 'moment';
import {DeliveryService} from "../api/delivery.service";
import {TimesReportService} from "../api/times-reports.service";
import {PurchaseService} from "../api/purchase.service";
import {IData, ITimesreportUnitTime} from "../../models/api/utils.models";
import {ProjectService} from "../api/project.service";
import {OrderService} from "../api/order.service";
import {InvoiceService} from "../api/invoice.service";
import {AlertService} from "../alert/alert.service";
import {TimesReportWorkUnitTimeType} from "../../models/api/times-report-work-unit-time-type";
import {IMonthlySummaryTotalsAmountsData} from "../../models/data-table/monthly-summary-totals-amounts-data.model";

type EntityResponseType = HttpResponse<IBoondManagerResponse>;
type EntityArrayResponseType = HttpResponse<IBoondManagerArrayResponse>;

@Injectable({
  providedIn: 'root'
})
export class MonthlySummaryDataBuilderService {

  month: Moment = moment().startOf('month').subtract('1', 'months').startOf('month');
  currentMonth: Moment = moment().startOf('month');

  private data: IMonthlySummaryDataTableData[] = [];
  private dataSubject = new ReplaySubject<{ totalsAmountsData: IMonthlySummaryTotalsAmountsData, monthlySummaryDataTableData: IMonthlySummaryDataTableData[], month: Moment }>(1);

  resources!: IBoondManagerArrayResponse;
  monthInvoicesIDs: number[] = [];
  monthInvoices: IBoondManagerResponse[] = [];

  includeInvoices: IBoondManagerResponse[] = [];
  excludeInvoices: IBoondManagerResponse[] = [];

  eyaArray: IMonthlySummaryDataTableData[] = [];

  constructor(
    private invoiceService: InvoiceService,
    private orderService: OrderService,
    private projectService: ProjectService,
    private purchaseService: PurchaseService,
    private timesReportService: TimesReportService,
    private deliveryService: DeliveryService,
    private resourceService: ResourceService,
    private alertService: AlertService
  ) {
  }

  getData(): Observable<{ totalsAmountsData: IMonthlySummaryTotalsAmountsData, monthlySummaryDataTableData: IMonthlySummaryDataTableData[], month: Moment }> {
    return this.dataSubject.asObservable();
  }

  setMonth(newMonth: Moment, reloadData?: boolean): void {
    this.month = newMonth;
    if (reloadData) {
      this.buildDataTableData();
    }
  }

  buildDataTableData(): void {
    this.data = [];
    this.monthInvoicesIDs = [];
    this.monthInvoices = [];
    this.includeInvoices = [];
    this.excludeInvoices = [];
    // this.spinner.show();
    this.resourceService.getAllResources().subscribe(
      (response: HttpResponse<IBoondManagerArrayResponse>) => {
        this.resources = response.body!;
        if (this.resources.data?.length !== 0) {
          this.addDataFromResources(this.resources);
        } else {
          this.broadcastData();
        }
      },
      error => this.handleHttpError(error)
    );
  }

  private generateCommercialFullName(res: EntityResponseType): string | undefined {
    const commercialLastName: string | undefined = res.body?.included?.find(inc => inc.type === DataType.RESOURCE && inc.id === res.body?.data?.relationships?.mainManager?.data?.id)?.attributes?.lastName;
    const commercialFirstName: string | undefined = res.body?.included?.find(inc => inc.type === DataType.RESOURCE && inc.id === res.body?.data?.relationships?.mainManager?.data?.id)?.attributes?.firstName;
    if (commercialLastName && commercialFirstName) {
      return `${commercialLastName} ${commercialFirstName}`;
    } else if (commercialLastName) {
      return commercialLastName;
    } else if (commercialFirstName) {
      return commercialFirstName;
    } else {
      return undefined;
    }
  }

  private broadcastData(): void {
    // this.data = this.month.isSame(this.currentMonth, 'month') ? this.data : this.data.filter(item => (item.customerInvoiceReceivedHT && item.customerInvoiceReceivedTTC) || (item.supplierInvoiceReceivedHT && item.supplierInvoiceReceivedTTC));
    this.calculate();
    const totalsAmounts: IMonthlySummaryTotalsAmountsData = {
      marginTotal: Number(this.data.reduce((previousValue: number, currentItem: IMonthlySummaryDataTableData) => previousValue + (currentItem.margin ? currentItem.margin : 0), 0).toFixed(2)),
      forecastCustomerBillingHTTotal: Number(this.data.reduce((previousValue: number, currentItem: IMonthlySummaryDataTableData) => previousValue + (currentItem.forecastCustomerBillingHT || 0), 0).toFixed(2)),
      customerInvoiceReceivedHTTotal: (this.monthInvoices && this.monthInvoices.length > 0) ? Number(this.monthInvoices.reduce((previousValue: number, currentItem: IBoondManagerResponse) => previousValue + currentItem.data?.attributes?.totalExcludingTax!, 0)) : Number(this.data.reduce((previousValue: number, currentItem: IMonthlySummaryDataTableData) => previousValue + (currentItem.customerInvoiceReceivedHT || 0), 0).toFixed(2)),
      customerInvoiceReceivedTTCTotal: (this.monthInvoices && this.monthInvoices.length > 0) ? Number(this.monthInvoices.reduce((previousValue: number, currentItem: IBoondManagerResponse) => previousValue + currentItem.data?.attributes?.totalIncludingTax!, 0)) : Number(this.data.reduce((previousValue: number, currentItem: IMonthlySummaryDataTableData) => previousValue + (currentItem.customerInvoiceReceivedTTC || 0), 0).toFixed(2)),
      provisionalInvoicingSupplierHTTotal: Number(this.data.reduce((previousValue: number, currentItem: IMonthlySummaryDataTableData) => previousValue + (currentItem.provisionalInvoicingSupplierHT || 0), 0).toFixed(2)),
      supplierInvoiceReceivedHTTotal: Number(this.data.reduce((previousValue: number, currentItem: IMonthlySummaryDataTableData) => previousValue + (currentItem.supplierInvoiceReceivedHT || 0), 0).toFixed(2)),
      supplierInvoiceReceivedTTCTotal: Number(this.data.reduce((previousValue: number, currentItem: IMonthlySummaryDataTableData) => previousValue + (currentItem.supplierInvoiceReceivedTTC || 0), 0).toFixed(2)),
    };
    this.dataSubject.next({
      totalsAmountsData: totalsAmounts,
      monthlySummaryDataTableData: this.data,
      month: this.month
    });
    // this.spinner.hide();
    this.analyzeData();
    this.test();
    console.log('----------------------this.monthInvoices');
    console.log(this.monthInvoices);
    console.log('----------------------includedInvoices');
    console.log(this.includeInvoices);
    console.log('----------------------this.excludeInvoices');
    console.log(this.excludeInvoices);
    // console.log('----------------------custom this.monthInvoices');
    // console.log(this.monthInvoices.filter(item => item.data?.attributes?.activityDetails?.find(it => it.resource?.lastName === 'FERRARI')));
  }

  private handleHttpError(error: any): void {
    console.log(error);
    // this.spinner.hide();
  }

  private addDataFromResources(resources: IBoondManagerArrayResponse): void {
    if (!resources.data) {
      return;
    }
    let array: Observable<EntityResponseType>[] = [];
    resources.data.forEach(item => {
      const itemObservable: Observable<EntityResponseType> = this.resourceService.getResourceInformation(item.id!)
        .pipe(tap((res: EntityResponseType) => this.initializeDataResource(res)));
      array.push(itemObservable);
    });
    if (array.length > 0) {
      forkJoin(array).subscribe(res => {
        this.data.forEach(item => {
          this.eyaArray.push(JSON.parse(JSON.stringify(item)));
        });
        console.log('---------eyaArray');
        console.log(this.eyaArray);
        this.data = this.data.filter(row => row.isEmployee !== undefined);
        let array2: Observable<EntityArrayResponseType>[] = [];
        this.data.forEach(item => {
          const itemObservable2: Observable<EntityArrayResponseType> = this.resourceService.getResourceDeliveriesInformation(item.consultantID!)
            .pipe(tap((res: EntityArrayResponseType) => this.checkResourceBenefits(item.consultantID!, res)));
          array2.push(itemObservable2);
        });
        if (array2.length > 0) {
          forkJoin(array2).subscribe(res => {
            this.data = this.data.filter(row => row.hasBenifits && row.hasBenifitsForThisMonth);
            let array3: Observable<EntityArrayResponseType>[] = [];
            this.data.forEach(item => {
              const itemObservable3: Observable<EntityArrayResponseType> = this.deliveryService.find(item.benifitIdForThisMonth!)
                .pipe(
                  tap((res: EntityResponseType) => this.updateResourceBenefitInformation(item.consultantID!, res)),
                  concatMap((res: EntityResponseType) => this.resourceService.getResourceTimesReportsInformation(item.consultantID!)),
                  tap((res: EntityArrayResponseType) => this.checkResourceTimesReports(item.consultantID!, res))
                );
              array3.push(itemObservable3);
            });
            if (array3.length > 0) {
              forkJoin(array3).subscribe(res => {
                let array4: Observable<EntityResponseType>[] = [];
                this.data.filter(rsc => rsc.hasTimesReportForThisMonth).forEach(item => {
                  const itemObservable4: Observable<EntityResponseType> = this.timesReportService.find(item.timesReportIdForThisMonth!)
                    .pipe(
                      tap((res: EntityResponseType) => this.updateResourceTimesReportInformation(item.consultantID!, res)),
                    );
                  array4.push(itemObservable4);
                });
                if (array4.length > 0) {
                  forkJoin(array4).subscribe(res => {
                    let array5: Observable<EntityArrayResponseType>[] = [];
                    this.data.filter(rsc => rsc.purchaseID).forEach(item => {
                      const itemObservable5: Observable<EntityArrayResponseType> = this.purchaseService.getPurchasePaymentsInformation(item.purchaseID!)
                        .pipe(tap((res: EntityArrayResponseType) => this.updateResourceSupplierInvoicingInformation(item.consultantID!, res)));
                      array5.push(itemObservable5);
                    });
                    if (array5.length > 0) {
                      forkJoin(array5).subscribe(res => {
                        let array6: Observable<EntityArrayResponseType>[] = [];
                        const itemObservable6: Observable<EntityArrayResponseType> = this.invoiceService.getInvoicesByStartDateAndEndDate(this.month.clone().startOf('month').format('YYYY-MM-DD'), this.month.clone().endOf('month').format('YYYY-MM-DD'))
                          .pipe(tap((res: EntityArrayResponseType) => this.monthInvoicesIDs = res.body?.data?.map(item => item.id!) || []));
                        array6.push(itemObservable6);
                        if (array6.length > 0) {
                          forkJoin(array6).subscribe(res => {
                            let array7: Observable<EntityResponseType>[] = [];
                            this.monthInvoicesIDs.forEach(invoiceID => {
                              const itemObservable7: Observable<EntityResponseType> = this.invoiceService.getInvoiceInformation(invoiceID)
                                .pipe(tap((res: EntityResponseType) => this.monthInvoices.push(res.body!)));
                              array7.push(itemObservable7);
                            });
                            if (array7.length > 0) {
                              forkJoin(array7).subscribe(res => {
                                this.fillMonthCustomerInvoicesInformation();
                                this.broadcastData();
                              });
                            } else {
                              console.error('observable array 7 error');
                              this.broadcastData();
                            }
                          });
                        } else {
                          console.error('observable array 6 error');
                          this.broadcastData();
                        }
                      });
                    } else {
                      console.error('observable array 5 error');
                      this.broadcastData();
                    }
                  });
                } else {
                  console.error('observable array 4 error');
                  this.broadcastData();
                }
              });
            } else {
              console.error('observable array 3 error');
              this.broadcastData();
            }
          });
        } else {
          console.error('observable array 2 error');
          this.broadcastData();
        }
      });
    } else {
      console.error('observable array 1 error');
      this.broadcastData();
    }
  }

  private initializeDataResource(res: EntityResponseType): void {
    this.data.push({
      consultantID: res.body?.data?.id!,
      consultantFullName: `${res.body?.data?.attributes?.lastName} ${res.body?.data?.attributes?.firstName}`,
      isEmployee: (!((res.body?.data?.attributes?.typeOf === 1) || (res.body?.data?.attributes?.typeOf === 12))),
      commercialID: res.body?.data?.relationships?.mainManager?.data?.id,
      commercialFullName: this.generateCommercialFullName(res)
    });
  }

  private checkResourceBenefits(resourceID: number, benifits: EntityArrayResponseType): void {
    const indexItemToUpdate: number = this.data.findIndex(resource => resource.consultantID === resourceID);
    if (indexItemToUpdate !== -1) {
      const itemToUpdate: IMonthlySummaryDataTableData = this.data[indexItemToUpdate];
      itemToUpdate.hasBenifits = benifits.body?.data?.length! > 0;
      //itemToUpdate.hasBenifitsForThisMonth = itemToUpdate.hasBenifits && benifits.body?.data?.filter(item => this.month.isSame(moment(item.attributes?.startDate), 'month') || this.month.isSame(moment(item.attributes?.endDate), 'month')).length! > 0;
      //itemToUpdate.benifitIdForThisMonth = itemToUpdate.hasBenifitsForThisMonth ? benifits.body?.data?.find(item => this.month.isSame(moment(item.attributes?.startDate), 'month') || this.month.isSame(moment(item.attributes?.endDate), 'month'))?.id : undefined;
      itemToUpdate.hasBenifitsForThisMonth = benifits.body?.data?.filter(item => this.checkIfThisMonthIsIncludedInBenifitPeriod(moment(item.attributes?.startDate), moment(item.attributes?.endDate))).length! > 0;
      itemToUpdate.benifitIdForThisMonth = itemToUpdate.hasBenifitsForThisMonth ? benifits.body?.data?.find(item => this.checkIfThisMonthIsIncludedInBenifitPeriod(moment(item.attributes?.startDate), moment(item.attributes?.endDate)))?.id : undefined;
      itemToUpdate.projectIDForThisMonth = itemToUpdate.hasBenifitsForThisMonth ? benifits.body?.data?.find(item => this.month.isSame(moment(item.attributes?.startDate), 'month') || this.month.isSame(moment(item.attributes?.endDate), 'month'))?.relationships?.project?.data?.id : undefined
      this.data.splice(indexItemToUpdate, 1, itemToUpdate);
    }
  }

  private updateResourceBenefitInformation(resourceID: number, delivery: EntityResponseType): void {
    const indexItemToUpdate: number = this.data.findIndex(resource => resource.consultantID === resourceID);
    if (indexItemToUpdate !== -1) {
      const oldItem: IMonthlySummaryDataTableData = this.data[indexItemToUpdate];
      const neItem: IMonthlySummaryDataTableData = {
        ...oldItem,
        tjm: delivery.body?.data?.attributes?.averageDailyPriceExcludingTax,
        cjm: delivery.body?.data?.attributes?.averageDailyCost,
        finalCustomerID: delivery.body?.included?.find(inc => inc.type === DataType.COMPANY)?.id,
        finalCustomerName: delivery.body?.included?.find(inc => inc.type === DataType.COMPANY)?.attributes?.name,
        purchaseID: delivery.body?.data?.relationships?.purchase?.data?.id,
        numberDaysOrdered: delivery.body?.data?.attributes?.numberOfDaysInvoicedOrQuantity
      }
      this.data.splice(indexItemToUpdate, 1, neItem);
    }
  }

  private checkResourceTimesReports(resourceID: number, timesReports: EntityArrayResponseType): void {
    const indexItemToUpdate: number = this.data.findIndex(resource => resource.consultantID === resourceID);
    if (indexItemToUpdate !== -1) {
      const itemToUpdate: IMonthlySummaryDataTableData = this.data[indexItemToUpdate];
      itemToUpdate.hasTimesReportForThisMonth = timesReports.body?.data?.length! > 0 && timesReports.body?.data?.filter(item => item.attributes?.term === this.month.format('YYYY-MM').toString()).length! > 0;
      itemToUpdate.timesReportIdForThisMonth = itemToUpdate.hasTimesReportForThisMonth ? timesReports.body?.data?.find(item => item.attributes?.term === this.month.format('YYYY-MM').toString())?.id : undefined;
      itemToUpdate.timesReportStateForThisMonth = itemToUpdate.hasTimesReportForThisMonth ? timesReports.body?.data?.find(item => item.attributes?.term === this.month.format('YYYY-MM').toString())?.attributes?.state : undefined;
      itemToUpdate.monthProduction = itemToUpdate.hasTimesReportForThisMonth ? undefined : (this.month.daysInMonth() - 8);
      this.data.splice(indexItemToUpdate, 1, itemToUpdate);
    }
  }

  private updateResourceTimesReportInformation(resourceID: number, timesReport: EntityResponseType): void {
    const indexItemToUpdate: number = this.data.findIndex(resource => resource.consultantID === resourceID);
    if (indexItemToUpdate !== -1) {
      const oldItem: IMonthlySummaryDataTableData = this.data[indexItemToUpdate];
      // const neItem: IMonthlySummaryDataTableData = {
      //   ...oldItem,
      //   monthProduction: (timesReport.body?.data?.attributes?.regularTimes?.reduce((previousValue: number, currentItem: ITimesreportUnitTime) => previousValue + currentItem.duration!, 0)! - timesReport.body?.data?.attributes?.absencesTimes?.reduce((previousValue: number, currentItem: ITimesreportUnitTime) => previousValue + currentItem.duration!, 0)!)
      // };
      const neItem: IMonthlySummaryDataTableData = {
        ...oldItem,
        monthProduction: timesReport.body?.data?.attributes?.regularTimes?.filter(item => item.workUnitType?.activityType === TimesReportWorkUnitTimeType.PRODUCTION).reduce((previousValue: number, currentItem: ITimesreportUnitTime) => previousValue + currentItem.duration!, 0)!
      };
      this.data.splice(indexItemToUpdate, 1, neItem);
    }
  }

  // private calculateProvisionalInvoicingCustomerAndSupplier(): void {
  //   this.data.forEach(item => {
  //     item.forecastCustomerBillingHT = item.tjm! * item.monthProduction!;
  //     item.provisionalInvoicingSupplierHT = item.isEmployee ? undefined : item.cjm! * item.monthProduction!;
  //   });
  // }

  // private calculateMargin(): void {
  //   this.data.forEach(item => {
  //     if (!item.supplierPayment || item.supplierPayment === '') {
  //       item.supplierInvoiceReceivedHT = undefined;
  //       item.supplierInvoiceReceivedTTC = undefined;
  //       item.supplierPayment = undefined;
  //     }
  //     item.margin = item.isEmployee ? (item.customerInvoiceReceivedHT || undefined) : ((item.customerInvoiceReceivedHT || 0) - (item.supplierInvoiceReceivedHT || 0));
  //   });
  // }

  private calculate(): void {
    this.data.forEach(item => {
      item.orderRemainingDays = (item.numberDaysOrdered || 0) - (item.monthProduction || 0); // TODO check
      item.forecastCustomerBillingHT = item.tjm! * item.monthProduction!;
      // item.customerInvoiceReceivedHT = (!item.customerInvoiceReceivedHT && this.month.isSame(this.currentMonth, 'month')) ? item.tjm! * item.monthProduction! : item.customerInvoiceReceivedHT;
      item.provisionalInvoicingSupplierHT = item.isEmployee ? undefined : item.cjm! * item.monthProduction!;
      // item.supplierInvoiceReceivedHT = (!item.isEmployee && !item.supplierInvoiceReceivedHT && this.month.isSame(this.currentMonth, 'month')) ? item.cjm! * item.monthProduction! : item.supplierInvoiceReceivedHT;
      item.margin = item.isEmployee ? ((item.customerInvoiceReceivedHT || item.forecastCustomerBillingHT || 0) - ((item.cjm! * item.monthProduction!) || 0)) :
        ((item.customerInvoiceReceivedHT || item.forecastCustomerBillingHT || 0) - (item.supplierInvoiceReceivedHT || item.provisionalInvoicingSupplierHT || 0));
    });
  }

  private updateResourceSupplierInvoicingInformation(resourceID: number, payments: EntityArrayResponseType): void {
    const indexItemToUpdate: number = this.data.findIndex(resource => resource.consultantID === resourceID);
    if (indexItemToUpdate !== -1) {
      const monthPaymentData: IData[] = payments.body?.data?.filter(item => item.attributes?.startDate === this.month.startOf('month').format('YYYY-MM-DD') &&
        item.attributes?.endDate === this.month.endOf('month').format('YYYY-MM-DD'))!;
      const oldItem: IMonthlySummaryDataTableData = this.data[indexItemToUpdate];
      const neItem: IMonthlySummaryDataTableData = {
        ...oldItem,
        // supplierInvoiceReceivedHT: (monthPaymentData?.attributes?.performedDate && moment(monthPaymentData?.attributes?.performedDate).isValid()) ? monthPaymentData?.attributes?.amountExcludingTax : undefined,
        // supplierInvoiceReceivedTTC: (monthPaymentData?.attributes?.performedDate && moment(monthPaymentData?.attributes?.performedDate).isValid()) ? monthPaymentData?.attributes?.amountIncludingTax : undefined,
        // supplierPayment: (monthPaymentData?.attributes?.performedDate && moment(monthPaymentData?.attributes?.performedDate).isValid()) ? moment(monthPaymentData?.attributes?.performedDate).format('DD/MM/YYYY') : undefined
        supplierInvoiceReceivedHT: monthPaymentData.reduce((previousValue: number, currentItem: IData) => previousValue + ((currentItem?.attributes?.performedDate && moment(currentItem?.attributes?.performedDate).isValid()) ? currentItem?.attributes?.amountExcludingTax! : 0), 0),
        supplierInvoiceReceivedTTC: monthPaymentData.reduce((previousValue: number, currentItem: IData) => previousValue + ((currentItem?.attributes?.performedDate && moment(currentItem?.attributes?.performedDate).isValid()) ? currentItem?.attributes?.amountIncludingTax! : 0), 0),
        // supplierPayment: moment(monthPaymentData.slice().reverse().find((item: IData) => item?.attributes?.performedDate && moment(item?.attributes?.performedDate).isValid())?.attributes?.performedDate).format('DD/MM/YYYY') || undefined,
      }
      const newItemSupplierPayment: Moment = moment(monthPaymentData.slice().reverse().find((item: IData) => item?.attributes?.performedDate && moment(item?.attributes?.performedDate).isValid())?.attributes?.performedDate);
      neItem.supplierPayment = (neItem.supplierInvoiceReceivedHT && newItemSupplierPayment.isValid()) ? newItemSupplierPayment.format('DD/MM/YYYY') : undefined;
      this.data.splice(indexItemToUpdate, 1, neItem);
    }
  }

  private checkIfThisMonthIsIncludedInBenifitPeriod(benifitStartDate: Moment, benifitEndDate: Moment): boolean {
    const monthStartDate: Moment = this.month.clone().startOf('month');
    const monthEndDate: Moment = this.month.clone().endOf('month');
    return ((benifitStartDate.isSameOrBefore(monthStartDate, 'day') && (((benifitEndDate.isSameOrAfter(monthStartDate, 'day')) && (benifitEndDate.isSameOrBefore(monthEndDate, 'day'))) || benifitEndDate.isSameOrAfter(monthEndDate, 'day'))) ||
      (((benifitStartDate.isSameOrAfter(monthStartDate, 'day')) && (benifitStartDate.isSameOrBefore(monthEndDate, 'day'))) && (benifitEndDate.isSameOrBefore(monthEndDate, 'day') || benifitEndDate.isSameOrAfter(monthEndDate, 'day'))));
  }

  private updateResourceProjectOrderInformation(resource: IMonthlySummaryDataTableData, projectOrders: EntityArrayResponseType): void {
    const order: IData | undefined = projectOrders.body?.data?.find(item => this.month.startOf('month').isSame(item.attributes?.startDate) || this.month.endOf('month').isSame(item.attributes?.endDate));
    resource.hasOrderForThisMonth = order !== undefined;
    resource.orderIdForThisMonth = order?.id;
  }

  private updateResourceProjectInvoiceInformation(resource: IMonthlySummaryDataTableData, orderInvoices: EntityArrayResponseType): void {
    const invoice: IData | undefined = (orderInvoices.body?.data && orderInvoices.body?.data?.length > 0) ? orderInvoices.body?.data[0] : undefined;
    resource.hasInvoiceForThisMonth = invoice !== undefined;
    resource.invoiceIdForThisMonth = invoice?.id;
  }

  private updateResourceCustomerInvoicingInformation(resource: IMonthlySummaryDataTableData, invoice: EntityResponseType): void {
    resource.customerInvoiceReceivedHT = invoice.body?.data?.attributes?.totalExcludingTax;
    resource.customerInvoiceReceivedTTC = invoice.body?.data?.attributes?.totalIncludingTax;
    resource.customerPayment = invoice.body?.data?.attributes?.performedPaymentDate !== '' ? moment(invoice.body?.data?.attributes?.performedPaymentDate).format('DD/MM/YYYY') : undefined
  }

  private analyzeData(): void {
    this.data.forEach(row => {
      if (row.customerInvoiceReceivedHT! < row.forecastCustomerBillingHT!) {
        this.alertService.showProvisionalCustomerInvoiceHtAmountIsLessThanCustomerInvoiceHtAmount(row.consultantFullName!);
      }
      if (row.customerInvoiceReceivedHT! < row.supplierInvoiceReceivedHT!) {
        this.alertService.showCustomerInvoiceAmountIsLessThanProviderInvoiceAmount(row.consultantFullName!);
      }

      // if (row.margin! < 0 ) {
      //   this.alertService.showCustomerInvoiceAmountIsLessThanProviderInvoiceAmount(row.consultantFullName!);
      // }
    });
  }

  // private fillMonthCustomerInvoicesInformation(): void {
  //   this.data.forEach(row => {
  //     this.monthInvoices.forEach(invoice => {
  //       if (invoice.data?.attributes?.activityDetails?.find(item => item.resource?.id === row.consultantID) &&
  //         invoice.included?.find(item => item.type === DataType.COMPANY && item.id === row.finalCustomerID)) {
  //         // row.customerInvoiceReceivedHT = ((row.customerInvoiceReceivedHT || 0) + invoice.data?.attributes?.totalExcludingTax!);
  //         row.customerInvoiceReceivedHT = row.customerInvoiceReceivedHT ? (row.customerInvoiceReceivedHT + invoice.data?.attributes?.totalExcludingTax!) : (invoice.data?.attributes?.totalExcludingTax!);
  //         row.customerInvoiceReceivedTTC = row.customerInvoiceReceivedTTC ? (row.customerInvoiceReceivedTTC + invoice.data?.attributes?.totalIncludingTax!) : (invoice.data?.attributes?.totalIncludingTax!);
  //         // row.customerInvoiceReceivedTTC = ((row.customerInvoiceReceivedTTC || 0) + invoice.data?.attributes?.totalIncludingTax!);
  //         row.customerPayment = invoice.data?.attributes?.expectedPaymentDate !== '' ? moment(invoice.data?.attributes?.expectedPaymentDate).format('DD/MM/YYYY') : undefined
  //       }
  //     });
  //   });
  // }

  private fillMonthCustomerInvoicesInformation(): void {
    this.data.forEach(row => {

      const rowInvoices: IBoondManagerResponse[] = this.monthInvoices.filter(invoice => {
        return (invoice.data?.attributes?.activityDetails?.find(item => item.resource?.id === row.consultantID) &&
          invoice.included?.find(item => item.type === DataType.COMPANY && item.id === row.finalCustomerID) &&
          (invoice.data?.attributes?.state == 1 || invoice.data?.attributes?.state == 3)); // invoice_state_1 = "transmitted_to_the_client" && invoice_state_3 = "payed"
      });
      const rowInvoicesIDs: number[] = rowInvoices.map(invoice => invoice.data?.id!);
      const rowInvoicesOrdersIDs: number[] = rowInvoices.map(invoice => invoice.data?.relationships?.order?.data?.id!);
      const rowInvoicesHaves: IBoondManagerResponse[] = this.monthInvoices.filter(invoice => {
        return (rowInvoicesOrdersIDs.includes(invoice.data?.relationships?.order?.data?.id!)) &&
          (!rowInvoicesIDs.includes(invoice.data?.id!));
      });

      row.customerInvoiceReceivedHT = (rowInvoices && rowInvoices.length > 0) ?
        ((rowInvoices.reduce((previousValue: number, currentItem: IBoondManagerResponse) => previousValue + currentItem.data?.attributes?.totalExcludingTax!, 0) || 0) +
          (rowInvoicesHaves.reduce((previousValue: number, currentItem: IBoondManagerResponse) => previousValue + currentItem.data?.attributes?.totalExcludingTax!, 0) || 0)) :
        undefined;

      row.customerInvoiceReceivedTTC = (rowInvoices && rowInvoices.length > 0) ?
        ((rowInvoices.reduce((previousValue: number, currentItem: IBoondManagerResponse) => previousValue + currentItem.data?.attributes?.totalIncludingTax!, 0) || 0) +
          (rowInvoicesHaves.reduce((previousValue: number, currentItem: IBoondManagerResponse) => previousValue + currentItem.data?.attributes?.totalIncludingTax!, 0) || 0)) :
        undefined;

      // row.customerInvoiceReceivedHT = (rowInvoicesHaves && rowInvoicesHaves.length > 0) ?
      //   (row.customerInvoiceReceivedHT || 0) + (rowInvoicesHaves.reduce((previousValue: number, currentItem: IBoondManagerResponse) => previousValue + currentItem.data?.attributes?.totalExcludingTax!, 0) || 0) :
      //   row.customerInvoiceReceivedHT;
      // row.customerInvoiceReceivedTTC = (rowInvoicesHaves && rowInvoicesHaves.length > 0) ?
      //   (row.customerInvoiceReceivedTTC || 0) + (rowInvoicesHaves.reduce((previousValue: number, currentItem: IBoondManagerResponse) => previousValue + currentItem.data?.attributes?.totalIncludingTax!, 0) || 0) :
      //   row.customerInvoiceReceivedTTC;
      row.customerPayment = rowInvoices[(rowInvoices.length - 1)] && rowInvoices[(rowInvoices.length - 1)].data?.attributes?.performedPaymentDate !== '' ? moment(rowInvoices[(rowInvoices.length - 1)].data?.attributes?.performedPaymentDate).format('DD/MM/YYYY') : undefined;

    });
  }

  test(): void {
    const resourcesIDs: number[] = this.data.map(item => item.consultantID!);
    this.monthInvoices.forEach(invoice => {
      if (invoice.data?.attributes?.activityDetails?.find(item => resourcesIDs.includes(item.resource?.id!))) {
        this.includeInvoices.push(invoice);
      } else {
        this.excludeInvoices.push(invoice);
      }
    })
  }

}
