import {Injectable} from '@angular/core';
import {HttpResponse} from "@angular/common/http";
import {IBoondManagerResponse} from "../../models/api/boondmanager-response.model";
import {IBoondManagerArrayResponse} from "../../models/api/boondmanager-array-response.model";
import {forkJoin, Observable, ReplaySubject} from "rxjs";
import {NgxSpinnerService} from "ngx-spinner";
import * as moment from 'moment';
import {Moment} from 'moment';
import {
  IAnnualSummaryDataTableData,
  IInvoice,
  IMonthData, IPayment
} from "../../models/data-table/annual-summary-data-table-data.model";
import {CompanyService} from "../api/company.service";
import {concatMap, tap} from "rxjs/operators";
import {IData} from "../../models/api/utils.models";
import {InvoiceService} from "../api/invoice.service";
import {OrderService} from "../api/order.service";
import {ProjectService} from "../api/project.service";
import {PurchaseService} from "../api/purchase.service";

type EntityResponseType = HttpResponse<IBoondManagerResponse>;
type EntityArrayResponseType = HttpResponse<IBoondManagerArrayResponse>;

@Injectable({
  providedIn: 'root'
})
export class AnnualSummaryDataBuilderService {

  year: Moment = moment().startOf('year');

  private data: IAnnualSummaryDataTableData[] = [];
  private dataSubject = new ReplaySubject<IAnnualSummaryDataTableData[]>(1);

  resources!: IBoondManagerArrayResponse;

  constructor(
    private purchaseService: PurchaseService,
    private projectService: ProjectService,
    private companyService: CompanyService,
    private invoiceService: InvoiceService,
    private orderService: OrderService,
    private spinner: NgxSpinnerService
  ) {
  }

  getData(): Observable<IAnnualSummaryDataTableData[]> {
    return this.dataSubject.asObservable();
  }

  setYear(newYear: Moment, reloadData?: boolean): void {
    this.year = newYear;
    if (reloadData) {
      this.buildDataTableData();
    }
  }

  buildDataTableData(): void {
    this.data = [];
    this.spinner.show();
    this.companyService.getAllClientCompanies().subscribe(
      (response: HttpResponse<IBoondManagerArrayResponse>) => {
        this.resources = response.body!;
        if (this.resources.data?.length !== 0) {
          this.addDataFromCompanies(this.resources);
        } else {
          this.broadcastData();
        }
      },
      error => this.handleHttpError(error)
    );
  }

  private broadcastData(): void {
    this.calculate();
    this.data = this.data.filter(item => item.totalYearInvoicesAmountHT);
    this.dataSubject.next(this.data);
    this.spinner.hide();
  }

  private handleHttpError(error: any): void {
    console.log(error);
    this.spinner.hide();
  }

  private addDataFromCompanies(companies: IBoondManagerArrayResponse): void {
    if (!companies.data) {
      return;
    }
    let array1: Observable<EntityArrayResponseType>[] = [];
    companies.data.forEach(item => {
      this.initializeDataCompanies(item);
      const itemObservable1: Observable<EntityArrayResponseType> = this.companyService.getCompanyInvoicesInformation(item.id!)
        .pipe(tap((res: EntityArrayResponseType) => this.fillCompanyInvoicesInformation(item.id!, res)));
      array1.push(itemObservable1);
    });
    if (array1.length > 0) {
      forkJoin(array1).subscribe(res => {
        let array2: Observable<EntityResponseType>[] = [];
        this.data.forEach((comp: IAnnualSummaryDataTableData) => {
          comp.invoices?.forEach((invoice: IInvoice) => {
            const itemObservable2: Observable<EntityResponseType> = this.invoiceService.getInvoiceInformation(invoice.invoiceID!)
              .pipe(
                tap((res: EntityResponseType) => {
                  invoice.invoiceAmountHT = res.body?.data?.attributes?.totalExcludingTax;
                  invoice.invoiceAmountTTC = res.body?.data?.attributes?.totalIncludingTax;
                  invoice.startDate = res.body?.data?.attributes?.startDate;
                  invoice.endDate = res.body?.data?.attributes?.endDate;
                })
              );
            array2.push(itemObservable2);
          });
        });
        if (array2.length > 0) {
          forkJoin(array2).subscribe(res => {
            this.updateCompanyInvoicesInformation();
            let array3: Observable<EntityArrayResponseType>[] = [];
            this.data.forEach(row => {
              const itemObservable3: Observable<EntityArrayResponseType> = this.companyService.getCompanyProjectsInformation(row.clientID!)
                .pipe(tap((res: EntityArrayResponseType) => res.body?.data?.forEach(project => row.projects?.push({
                  projectID: project.id,
                  purchasesIDs: []
                }))));
              array3.push(itemObservable3);
            });
            if (array3.length > 0) {
              forkJoin(array3).subscribe(res => {
                let array4: Observable<EntityArrayResponseType>[] = [];
                this.data.forEach(row => {
                  row.projects?.filter(proj => proj.projectID != 28).forEach(project => {
                    const itemObservable4: Observable<EntityArrayResponseType> = this.projectService.getProjectDeliveriesInformation(project.projectID!)
                      .pipe(tap((res: EntityArrayResponseType) => project.hasPurchases = res.body?.data?.find(item => item.relationships?.purchase?.data?.id) !== undefined));
                    array4.push(itemObservable4);
                  });
                });
                if (array4.length > 0) {
                  forkJoin(array4).subscribe(res => {
                    let array5: Observable<EntityArrayResponseType>[] = [];
                    this.data.forEach(row => {
                      row.projects?.filter(proj => proj.hasPurchases).forEach(project => {
                        const itemObservable5: Observable<EntityArrayResponseType> = this.projectService.getProjectPurchasesInformation(project.projectID!)
                          .pipe(tap((res: EntityArrayResponseType) => res.body?.data?.forEach(purchase => project.purchasesIDs?.push(purchase.id!))));
                        array5.push(itemObservable5);
                      });
                    });
                    if (array5.length > 0) {
                      forkJoin(array5).subscribe(res => {
                        let array6: Observable<EntityArrayResponseType>[] = [];
                        this.data.forEach(row => {
                          row.projects?.filter(proj => proj.hasPurchases).forEach(project => {
                            project.purchasesIDs?.forEach(purchaseID => {
                              const itemObservable6: Observable<EntityArrayResponseType> = this.purchaseService.getPurchasePaymentsInformation(purchaseID)
                                .pipe(tap((res: EntityArrayResponseType) => this.updateCompanyPaymentsInformation(row, res)));
                              array6.push(itemObservable6);
                            });
                          });
                        });
                        if (array6.length > 0) {
                          forkJoin(array6).subscribe(res => {
                            this.broadcastData();
                          });
                        } else {
                          console.error('observable array 6 error');
                          this.broadcastData();
                        }
                      });
                    } else {
                      console.error('observable array 5 error');
                      this.broadcastData();
                    }
                  });
                } else {
                  console.error('observable array 4 error');
                  this.broadcastData();
                }
              });
            } else {
              console.error('observable array 3 error');
              this.broadcastData();
            }
          });
        } else {
          console.error('observable array 2 error');
          this.broadcastData();
        }
      });
    } else {
      console.error('observable array 1 error');
      this.broadcastData();
    }
  }

  private initializeDataCompanies(company: IData): void {
    this.data.push({
      clientID: company.id,
      clientName: company.attributes?.name,
      totalYearMarginHT: 0,
      totalYearInvoicesAmountTTC: 0,
      summary: [],
      projects: [],
      invoices: [],
    });
  }

  private fillCompanyInvoicesInformation(companyID: number, invoices: EntityArrayResponseType): void {
    const indexItemToUpdate: number = this.data.findIndex(company => company.clientID === companyID);
    if (indexItemToUpdate !== -1) {
      const itemToUpdate: IAnnualSummaryDataTableData = this.data[indexItemToUpdate];
      itemToUpdate.invoices = invoices.body?.data?.map(invoice => {
        return {
          invoiceID: invoice.id,
          orderID: invoice.relationships?.order?.data?.id
        }
      });
      this.data.splice(indexItemToUpdate, 1, itemToUpdate);
    }
  }

  private updateCompanyInvoicesInformation(): void {
    this.data.forEach((itemToUpdate: IAnnualSummaryDataTableData) => {


      // const januaryInvoices: IData[] = invoices.body?.data?.filter(item => item.attributes?.state && typeof item.attributes?.state === 'number' && item.attributes?.state === 3 && this.year.isSame(moment(item.attributes.date), 'month'))!;
      const januaryInvoices: IInvoice[] = itemToUpdate.invoices?.filter(item => this.year.clone().startOf('month').format('YYYY-MM-DD') === item.startDate && this.year.clone().endOf('month').format('YYYY-MM-DD') === item.endDate)!;
      const februaryInvoices: IInvoice[] = itemToUpdate.invoices?.filter(item => this.year.clone().add('1', 'months').startOf('month').format('YYYY-MM-DD') === item.startDate && this.year.clone().add('1', 'months').endOf('month').format('YYYY-MM-DD') === item.endDate)!;
      const marchInvoices: IInvoice[] = itemToUpdate.invoices?.filter(item => this.year.clone().add('2', 'months').startOf('month').format('YYYY-MM-DD') === item.startDate && this.year.clone().add('2', 'months').endOf('month').format('YYYY-MM-DD') === item.endDate)!;
      const aprilInvoices: IInvoice[] = itemToUpdate.invoices?.filter(item => this.year.clone().add('3', 'months').startOf('month').format('YYYY-MM-DD') === item.startDate && this.year.clone().add('3', 'months').endOf('month').format('YYYY-MM-DD') === item.endDate)!;
      const mayInvoices: IInvoice[] = itemToUpdate.invoices?.filter(item => this.year.clone().add('4', 'months').startOf('month').format('YYYY-MM-DD') === item.startDate && this.year.clone().add('4', 'months').endOf('month').format('YYYY-MM-DD') === item.endDate)!;
      const juneInvoices: IInvoice[] = itemToUpdate.invoices?.filter(item => this.year.clone().add('5', 'months').startOf('month').format('YYYY-MM-DD') === item.startDate && this.year.clone().add('5', 'months').endOf('month').format('YYYY-MM-DD') === item.endDate)!;
      const julyInvoices: IInvoice[] = itemToUpdate.invoices?.filter(item => this.year.clone().add('6', 'months').startOf('month').format('YYYY-MM-DD') === item.startDate && this.year.clone().add('6', 'months').endOf('month').format('YYYY-MM-DD') === item.endDate)!;
      const augustInvoices: IInvoice[] = itemToUpdate.invoices?.filter(item => this.year.clone().add('7', 'months').startOf('month').format('YYYY-MM-DD') === item.startDate && this.year.clone().add('7', 'months').endOf('month').format('YYYY-MM-DD') === item.endDate)!;
      const septemberInvoices: IInvoice[] = itemToUpdate.invoices?.filter(item => this.year.clone().add('8', 'months').startOf('month').format('YYYY-MM-DD') === item.startDate && this.year.clone().add('8', 'months').endOf('month').format('YYYY-MM-DD') === item.endDate)!;
      const octoberInvoices: IInvoice[] = itemToUpdate.invoices?.filter(item => this.year.clone().add('9', 'months').startOf('month').format('YYYY-MM-DD') === item.startDate && this.year.clone().add('9', 'months').endOf('month').format('YYYY-MM-DD') === item.endDate)!;
      const novemberInvoices: IInvoice[] = itemToUpdate.invoices?.filter(item => this.year.clone().add('10', 'months').startOf('month').format('YYYY-MM-DD') === item.startDate && this.year.clone().add('10', 'months').endOf('month').format('YYYY-MM-DD') === item.endDate)!;
      const decemberInvoices: IInvoice[] = itemToUpdate.invoices?.filter(item => this.year.clone().add('11', 'months').startOf('month').format('YYYY-MM-DD') === item.startDate && this.year.clone().add('11', 'months').endOf('month').format('YYYY-MM-DD') === item.endDate)!;

      itemToUpdate.summary?.push({
        monthID: 1,
        monthName: 'January',
        invoicesExists: januaryInvoices.length > 0,
        totalMonthMarginHT: 0,
        totalMonthMarginTTC: 0,
        totalMonthInvoicesAmountTTC: 0,
        totalMonthInvoicesAmountHT: 0,
        monthInvoices: januaryInvoices || [],
        payments: [],
        totalMonthPaymentsAmountHT: 0,
        totalMonthPaymentsAmountTTC: 0,
      });

      itemToUpdate.summary?.push({
        monthID: 2,
        monthName: 'February',
        invoicesExists: februaryInvoices.length > 0,
        totalMonthMarginHT: 0,
        totalMonthMarginTTC: 0,
        totalMonthInvoicesAmountTTC: 0,
        totalMonthInvoicesAmountHT: 0,
        monthInvoices: februaryInvoices || [],
        payments: [],
        totalMonthPaymentsAmountHT: 0,
        totalMonthPaymentsAmountTTC: 0,
      });

      itemToUpdate.summary?.push({
        monthID: 3,
        monthName: 'March',
        invoicesExists: marchInvoices.length > 0,
        totalMonthMarginHT: 0,
        totalMonthMarginTTC: 0,
        totalMonthInvoicesAmountTTC: 0,
        totalMonthInvoicesAmountHT: 0,
        monthInvoices: marchInvoices || [],
        payments: [],
        totalMonthPaymentsAmountHT: 0,
        totalMonthPaymentsAmountTTC: 0,
      });

      itemToUpdate.summary?.push({
        monthID: 4,
        monthName: 'April',
        invoicesExists: aprilInvoices.length > 0,
        totalMonthMarginHT: 0,
        totalMonthMarginTTC: 0,
        totalMonthInvoicesAmountTTC: 0,
        totalMonthInvoicesAmountHT: 0,
        monthInvoices: aprilInvoices || [],
        payments: [],
        totalMonthPaymentsAmountHT: 0,
        totalMonthPaymentsAmountTTC: 0,
      });

      itemToUpdate.summary?.push({
        monthID: 5,
        monthName: 'May',
        invoicesExists: mayInvoices.length > 0,
        totalMonthMarginHT: 0,
        totalMonthMarginTTC: 0,
        totalMonthInvoicesAmountTTC: 0,
        totalMonthInvoicesAmountHT: 0,
        monthInvoices: mayInvoices || [],
        payments: [],
        totalMonthPaymentsAmountHT: 0,
        totalMonthPaymentsAmountTTC: 0,
      });

      itemToUpdate.summary?.push({
        monthID: 6,
        monthName: 'June',
        invoicesExists: juneInvoices.length > 0,
        totalMonthMarginHT: 0,
        totalMonthMarginTTC: 0,
        totalMonthInvoicesAmountTTC: 0,
        totalMonthInvoicesAmountHT: 0,
        monthInvoices: juneInvoices || [],
        payments: [],
        totalMonthPaymentsAmountHT: 0,
        totalMonthPaymentsAmountTTC: 0,
      });

      itemToUpdate.summary?.push({
        monthID: 7,
        monthName: 'July',
        invoicesExists: julyInvoices.length > 0,
        totalMonthMarginHT: 0,
        totalMonthMarginTTC: 0,
        totalMonthInvoicesAmountTTC: 0,
        totalMonthInvoicesAmountHT: 0,
        monthInvoices: julyInvoices || [],
        payments: [],
        totalMonthPaymentsAmountHT: 0,
        totalMonthPaymentsAmountTTC: 0,
      });

      itemToUpdate.summary?.push({
        monthID: 8,
        monthName: 'August',
        invoicesExists: augustInvoices.length > 0,
        totalMonthMarginHT: 0,
        totalMonthMarginTTC: 0,
        totalMonthInvoicesAmountTTC: 0,
        totalMonthInvoicesAmountHT: 0,
        monthInvoices: augustInvoices || [],
        payments: [],
        totalMonthPaymentsAmountHT: 0,
        totalMonthPaymentsAmountTTC: 0,
      });

      itemToUpdate.summary?.push({
        monthID: 9,
        monthName: 'September',
        invoicesExists: septemberInvoices.length > 0,
        totalMonthMarginHT: 0,
        totalMonthMarginTTC: 0,
        totalMonthInvoicesAmountTTC: 0,
        totalMonthInvoicesAmountHT: 0,
        monthInvoices: septemberInvoices || [],
        payments: [],
        totalMonthPaymentsAmountHT: 0,
        totalMonthPaymentsAmountTTC: 0,
      });

      itemToUpdate.summary?.push({
        monthID: 10,
        monthName: 'October',
        invoicesExists: octoberInvoices.length > 0,
        totalMonthMarginHT: 0,
        totalMonthMarginTTC: 0,
        totalMonthInvoicesAmountTTC: 0,
        totalMonthInvoicesAmountHT: 0,
        monthInvoices: octoberInvoices || [],
        payments: [],
        totalMonthPaymentsAmountHT: 0,
        totalMonthPaymentsAmountTTC: 0,
      });

      itemToUpdate.summary?.push({
        monthID: 11,
        monthName: 'November',
        invoicesExists: novemberInvoices.length > 0,
        totalMonthMarginHT: 0,
        totalMonthMarginTTC: 0,
        totalMonthInvoicesAmountTTC: 0,
        totalMonthInvoicesAmountHT: 0,
        monthInvoices: novemberInvoices || [],
        payments: [],
        totalMonthPaymentsAmountHT: 0,
        totalMonthPaymentsAmountTTC: 0,
      });

      itemToUpdate.summary?.push({
        monthID: 12,
        monthName: 'December',
        invoicesExists: decemberInvoices.length > 0,
        totalMonthMarginHT: 0,
        totalMonthMarginTTC: 0,
        totalMonthInvoicesAmountTTC: 0,
        totalMonthInvoicesAmountHT: 0,
        monthInvoices: decemberInvoices || [],
        payments: [],
        totalMonthPaymentsAmountHT: 0,
        totalMonthPaymentsAmountTTC: 0,
      });

    });
  }

  private calculate(): void {

    this.data.forEach((comp: IAnnualSummaryDataTableData) => {
      comp.summary?.filter(it => it.invoicesExists).forEach((sum: IMonthData) => {
        sum.totalMonthInvoicesAmountHT = sum.monthInvoices?.reduce((previousValue: number, currentItem: IInvoice) => previousValue + currentItem.invoiceAmountHT!, 0);
        sum.totalMonthInvoicesAmountTTC = sum.monthInvoices?.reduce((previousValue: number, currentItem: IInvoice) => previousValue + currentItem.invoiceAmountTTC!, 0);
      });
    });

    this.data.forEach((comp: IAnnualSummaryDataTableData) => {
      comp.summary?.filter(it => it.paymentsExists).forEach((sum: IMonthData) => {
        sum.totalMonthPaymentsAmountHT = sum.payments?.reduce((previousValue: number, currentItem: IPayment) => previousValue + currentItem.paymentAmountHT!, 0);
        sum.totalMonthPaymentsAmountTTC = sum.payments?.reduce((previousValue: number, currentItem: IPayment) => previousValue + currentItem.paymentAmountTTC!, 0);
      });
    });

    this.data.forEach((comp: IAnnualSummaryDataTableData) => {
      comp.summary?.forEach((sum: IMonthData) => {
        sum.totalMonthMarginHT = ((sum.totalMonthInvoicesAmountHT || 0) - (sum.totalMonthPaymentsAmountHT || 0));
        sum.totalMonthMarginTTC = ((sum.totalMonthInvoicesAmountTTC || 0) - (sum.totalMonthPaymentsAmountTTC || 0));
      });
    });

    this.data.forEach((comp: IAnnualSummaryDataTableData) => {
      comp.totalYearInvoicesAmountHT = comp.summary?.reduce((previousValue: number, currentItem: IMonthData) => previousValue + currentItem.totalMonthInvoicesAmountHT!, 0);
      comp.totalYearInvoicesAmountTTC = comp.summary?.reduce((previousValue: number, currentItem: IMonthData) => previousValue + currentItem.totalMonthInvoicesAmountTTC!, 0);
      comp.totalYearPaymentsAmountHT = comp.summary?.reduce((previousValue: number, currentItem: IMonthData) => previousValue + currentItem.totalMonthPaymentsAmountHT!, 0);
      comp.totalYearPaymentsAmountTTC = comp.summary?.reduce((previousValue: number, currentItem: IMonthData) => previousValue + currentItem.totalMonthPaymentsAmountTTC!, 0);
      comp.totalYearMarginHT = comp.summary?.reduce((previousValue: number, currentItem: IMonthData) => previousValue + currentItem.totalMonthMarginHT!, 0);
      comp.totalYearMarginTTC = comp.summary?.reduce((previousValue: number, currentItem: IMonthData) => previousValue + currentItem.totalMonthMarginTTC!, 0);
      comp.percentageTotalYearMarginHT = ((comp.totalYearMarginHT! * 100) / comp.totalYearInvoicesAmountHT!);
      comp.percentageTotalYearMarginTTC = ((comp.totalYearMarginTTC! * 100) / comp.totalYearInvoicesAmountTTC!);
    });
  }

  private updateCompanyPaymentsInformation(company: IAnnualSummaryDataTableData, payments: EntityArrayResponseType): void {

    const januaryPayments: IData[] = payments.body?.data?.filter((item: IData) => {
      return (moment(item.attributes?.performedDate).isValid() &&
        moment(item.attributes?.startDate).isSame(this.year.clone().startOf('month')) &&
        moment(item.attributes?.endDate).isSameOrBefore(this.year.clone().endOf('month')));
    })!;
    const februaryPayments: IData[] = payments.body?.data?.filter((item: IData) => {
      return (moment(item.attributes?.performedDate).isValid() &&
        moment(item.attributes?.startDate).isSame(this.year.clone().add('1', 'months').startOf('month')) &&
        moment(item.attributes?.endDate).isSameOrBefore(this.year.clone().add('1', 'months').endOf('month')));
    })!;
    const marchPayments: IData[] = payments.body?.data?.filter((item: IData) => {
      return (moment(item.attributes?.performedDate).isValid() &&
        moment(item.attributes?.startDate).isSame(this.year.clone().add('2', 'months').startOf('month')) &&
        moment(item.attributes?.endDate).isSameOrBefore(this.year.clone().add('2', 'months').endOf('month')));
    })!;
    const aprilPayments: IData[] = payments.body?.data?.filter((item: IData) => {
      return (moment(item.attributes?.performedDate).isValid() &&
        moment(item.attributes?.startDate).isSame(this.year.clone().add('3', 'months').startOf('month')) &&
        moment(item.attributes?.endDate).isSameOrBefore(this.year.clone().add('3', 'months').endOf('month')));
    })!;
    const mayPayments: IData[] = payments.body?.data?.filter((item: IData) => {
      return (moment(item.attributes?.performedDate).isValid() &&
        moment(item.attributes?.startDate).isSame(this.year.clone().add('4', 'months').startOf('month')) &&
        moment(item.attributes?.endDate).isSameOrBefore(this.year.clone().add('4', 'months').endOf('month')));
    })!;
    const junePayments: IData[] = payments.body?.data?.filter((item: IData) => {
      return (moment(item.attributes?.performedDate).isValid() &&
        moment(item.attributes?.startDate).isSame(this.year.clone().add('5', 'months').startOf('month')) &&
        moment(item.attributes?.endDate).isSameOrBefore(this.year.clone().add('5', 'months').endOf('month')));
    })!;
    const julyPayments: IData[] = payments.body?.data?.filter((item: IData) => {
      return (moment(item.attributes?.performedDate).isValid() &&
        moment(item.attributes?.startDate).isSame(this.year.clone().add('6', 'months').startOf('month')) &&
        moment(item.attributes?.endDate).isSameOrBefore(this.year.clone().add('6', 'months').endOf('month')));
    })!;
    const augustPayments: IData[] = payments.body?.data?.filter((item: IData) => {
      return (moment(item.attributes?.performedDate).isValid() &&
        moment(item.attributes?.startDate).isSame(this.year.clone().add('7', 'months').startOf('month')) &&
        moment(item.attributes?.endDate).isSameOrBefore(this.year.clone().add('7', 'months').endOf('month')));
    })!;
    const septemberPayments: IData[] = payments.body?.data?.filter((item: IData) => {
      return (moment(item.attributes?.performedDate).isValid() &&
        moment(item.attributes?.startDate).isSame(this.year.clone().add('8', 'months').startOf('month')) &&
        moment(item.attributes?.endDate).isSameOrBefore(this.year.clone().add('8', 'months').endOf('month')));
    })!;
    const octoberPayments: IData[] = payments.body?.data?.filter((item: IData) => {
      return (moment(item.attributes?.performedDate).isValid() &&
        moment(item.attributes?.startDate).isSame(this.year.clone().add('9', 'months').startOf('month')) &&
        moment(item.attributes?.endDate).isSameOrBefore(this.year.clone().add('9', 'months').endOf('month')));
    })!;
    const novemberPayments: IData[] = payments.body?.data?.filter((item: IData) => {
      return (moment(item.attributes?.performedDate).isValid() &&
        moment(item.attributes?.startDate).isSame(this.year.clone().add('10', 'months').startOf('month')) &&
        moment(item.attributes?.endDate).isSameOrBefore(this.year.clone().add('10', 'months').endOf('month')));
    })!;
    const decemberPayments: IData[] = payments.body?.data?.filter((item: IData) => {
      return (moment(item.attributes?.performedDate).isValid() &&
        moment(item.attributes?.startDate).isSame(this.year.clone().add('11', 'months').startOf('month')) &&
        moment(item.attributes?.endDate).isSameOrBefore(this.year.clone().add('11', 'months').endOf('month')));
    })!;

    if (januaryPayments.length > 0) {
      let januarySummary: IMonthData = company.summary?.find(sum => sum.monthID === 1)!;
      januarySummary.paymentsExists = true;
      januaryPayments.forEach((payment: IData) => januarySummary.payments?.push({
        paymentID: payment.id,
        startDate: payment.attributes?.startDate,
        endDate: payment.attributes?.endDate,
        paymentAmountHT: payment.attributes?.amountExcludingTax,
        paymentAmountTTC: payment.attributes?.amountIncludingTax,
        performedDate: payment.attributes?.performedDate
      }));
    }
    if (februaryPayments.length > 0) {
      let februarySummary: IMonthData = company.summary?.find(sum => sum.monthID === 2)!;
      februarySummary.paymentsExists = true;
      februaryPayments.forEach((payment: IData) => februarySummary.payments?.push({
        paymentID: payment.id,
        startDate: payment.attributes?.startDate,
        endDate: payment.attributes?.endDate,
        paymentAmountHT: payment.attributes?.amountExcludingTax,
        paymentAmountTTC: payment.attributes?.amountIncludingTax,
        performedDate: payment.attributes?.performedDate
      }));
    }
    if (marchPayments.length > 0) {
      let marchSummary: IMonthData = company.summary?.find(sum => sum.monthID === 3)!;
      marchSummary.paymentsExists = true;
      marchPayments.forEach((payment: IData) => marchSummary.payments?.push({
        paymentID: payment.id,
        startDate: payment.attributes?.startDate,
        endDate: payment.attributes?.endDate,
        paymentAmountHT: payment.attributes?.amountExcludingTax,
        paymentAmountTTC: payment.attributes?.amountIncludingTax,
        performedDate: payment.attributes?.performedDate
      }));
    }
    if (aprilPayments.length > 0) {
      let aprilSummary: IMonthData = company.summary?.find(sum => sum.monthID === 4)!;
      aprilSummary.paymentsExists = true;
      aprilPayments.forEach((payment: IData) => aprilSummary.payments?.push({
        paymentID: payment.id,
        startDate: payment.attributes?.startDate,
        endDate: payment.attributes?.endDate,
        paymentAmountHT: payment.attributes?.amountExcludingTax,
        paymentAmountTTC: payment.attributes?.amountIncludingTax,
        performedDate: payment.attributes?.performedDate
      }));
    }
    if (mayPayments.length > 0) {
      let maySummary: IMonthData = company.summary?.find(sum => sum.monthID === 5)!;
      maySummary.paymentsExists = true;
      mayPayments.forEach((payment: IData) => maySummary.payments?.push({
        paymentID: payment.id,
        startDate: payment.attributes?.startDate,
        endDate: payment.attributes?.endDate,
        paymentAmountHT: payment.attributes?.amountExcludingTax,
        paymentAmountTTC: payment.attributes?.amountIncludingTax,
        performedDate: payment.attributes?.performedDate
      }));
    }
    if (junePayments.length > 0) {
      let juneSummary: IMonthData = company.summary?.find(sum => sum.monthID === 6)!;
      juneSummary.paymentsExists = true;
      junePayments.forEach((payment: IData) => juneSummary.payments?.push({
        paymentID: payment.id,
        startDate: payment.attributes?.startDate,
        endDate: payment.attributes?.endDate,
        paymentAmountHT: payment.attributes?.amountExcludingTax,
        paymentAmountTTC: payment.attributes?.amountIncludingTax,
        performedDate: payment.attributes?.performedDate
      }));
    }
    if (julyPayments.length > 0) {
      let julySummary: IMonthData = company.summary?.find(sum => sum.monthID === 7)!;
      julySummary.paymentsExists = true;
      julyPayments.forEach((payment: IData) => julySummary.payments?.push({
        paymentID: payment.id,
        startDate: payment.attributes?.startDate,
        endDate: payment.attributes?.endDate,
        paymentAmountHT: payment.attributes?.amountExcludingTax,
        paymentAmountTTC: payment.attributes?.amountIncludingTax,
        performedDate: payment.attributes?.performedDate
      }));
    }
    if (augustPayments.length > 0) {
      let augustSummary: IMonthData = company.summary?.find(sum => sum.monthID === 8)!;
      augustSummary.paymentsExists = true;
      augustPayments.forEach((payment: IData) => augustSummary.payments?.push({
        paymentID: payment.id,
        startDate: payment.attributes?.startDate,
        endDate: payment.attributes?.endDate,
        paymentAmountHT: payment.attributes?.amountExcludingTax,
        paymentAmountTTC: payment.attributes?.amountIncludingTax,
        performedDate: payment.attributes?.performedDate
      }));
    }
    if (septemberPayments.length > 0) {
      let septemberSummary: IMonthData = company.summary?.find(sum => sum.monthID === 9)!;
      septemberSummary.paymentsExists = true;
      septemberPayments.forEach((payment: IData) => septemberSummary.payments?.push({
        paymentID: payment.id,
        startDate: payment.attributes?.startDate,
        endDate: payment.attributes?.endDate,
        paymentAmountHT: payment.attributes?.amountExcludingTax,
        paymentAmountTTC: payment.attributes?.amountIncludingTax,
        performedDate: payment.attributes?.performedDate
      }));
    }
    if (octoberPayments.length > 0) {
      let octoberSummary: IMonthData = company.summary?.find(sum => sum.monthID === 10)!;
      octoberSummary.paymentsExists = true;
      octoberPayments.forEach((payment: IData) => octoberSummary.payments?.push({
        paymentID: payment.id,
        startDate: payment.attributes?.startDate,
        endDate: payment.attributes?.endDate,
        paymentAmountHT: payment.attributes?.amountExcludingTax,
        paymentAmountTTC: payment.attributes?.amountIncludingTax,
        performedDate: payment.attributes?.performedDate
      }));
    }
    if (novemberPayments.length > 0) {
      let novemberSummary: IMonthData = company.summary?.find(sum => sum.monthID === 11)!;
      novemberSummary.paymentsExists = true;
      novemberPayments.forEach((payment: IData) => novemberSummary.payments?.push({
        paymentID: payment.id,
        startDate: payment.attributes?.startDate,
        endDate: payment.attributes?.endDate,
        paymentAmountHT: payment.attributes?.amountExcludingTax,
        paymentAmountTTC: payment.attributes?.amountIncludingTax,
        performedDate: payment.attributes?.performedDate
      }));
    }
    if (decemberPayments.length > 0) {
      let decemberSummary: IMonthData = company.summary?.find(sum => sum.monthID === 12)!;
      decemberSummary.paymentsExists = true;
      decemberPayments.forEach((payment: IData) => decemberSummary.payments?.push({
        paymentID: payment.id,
        startDate: payment.attributes?.startDate,
        endDate: payment.attributes?.endDate,
        paymentAmountHT: payment.attributes?.amountExcludingTax,
        paymentAmountTTC: payment.attributes?.amountIncludingTax,
        performedDate: payment.attributes?.performedDate
      }));
    }
  }

}
