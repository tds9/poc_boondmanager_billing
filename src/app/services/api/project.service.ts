import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpResponse} from "@angular/common/http";
import {JWT_CLIENT_HEADER, SERVER_API_URL} from "../../app.constants";
import {Observable} from "rxjs";
import {TokenProviderService} from "../token/token-provider.service";
import {IBoondManagerResponse} from "../../models/api/boondmanager-response.model";
import {IBoondManagerArrayResponse} from "../../models/api/boondmanager-array-response.model";

type EntityResponseType = HttpResponse<IBoondManagerResponse>;
type EntityArrayResponseType = HttpResponse<IBoondManagerArrayResponse>;

@Injectable({
  providedIn: 'root'
})
export class ProjectService {

  public resourceUrl = SERVER_API_URL + 'api/projects';

  constructor(
    protected http: HttpClient,
    protected tokenProviderService: TokenProviderService
  ) {
  }

  find(projectID: number): Observable<EntityResponseType> {
    let headers = new HttpHeaders();
    headers = headers.set(JWT_CLIENT_HEADER, this.tokenProviderService.generateToken());
    return this.http.get<IBoondManagerResponse>(`${this.resourceUrl}/${projectID}`, {
      headers: headers,
      observe: 'response'
    });
  }

  getProjectOrdersInformation(projectID: number): Observable<EntityArrayResponseType> {
    let headers = new HttpHeaders();
    headers = headers.set(JWT_CLIENT_HEADER, this.tokenProviderService.generateToken());
    return this.http.get<IBoondManagerArrayResponse>(`${this.resourceUrl}/${projectID}/orders`, {
      headers: headers,
      observe: 'response'
    });
  }

  getProjectPurchasesInformation(projectID: number): Observable<EntityArrayResponseType> {
    let headers = new HttpHeaders();
    headers = headers.set(JWT_CLIENT_HEADER, this.tokenProviderService.generateToken());
    return this.http.get<IBoondManagerArrayResponse>(`${this.resourceUrl}/${projectID}/purchases`, {
      headers: headers,
      observe: 'response'
    });
  }

  getProjectDeliveriesInformation(projectID: number): Observable<EntityArrayResponseType> {
    let headers = new HttpHeaders();
    headers = headers.set(JWT_CLIENT_HEADER, this.tokenProviderService.generateToken());
    return this.http.get<IBoondManagerArrayResponse>(`${this.resourceUrl}/${projectID}/deliveries-groupments`, {
      headers: headers,
      observe: 'response'
    });
  }

}
