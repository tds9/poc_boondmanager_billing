import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpResponse} from "@angular/common/http";
import {JWT_CLIENT_HEADER, SERVER_API_URL} from "../../app.constants";
import {Observable} from "rxjs";
import {TokenProviderService} from "../token/token-provider.service";
import {IBoondManagerResponse} from "../../models/api/boondmanager-response.model";
import {IBoondManagerArrayResponse} from "../../models/api/boondmanager-array-response.model";

type EntityResponseType = HttpResponse<IBoondManagerResponse>;
type EntityArrayResponseType = HttpResponse<IBoondManagerArrayResponse>;

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  public resourceUrl = SERVER_API_URL + 'api/orders';

  constructor(
    protected http: HttpClient,
    protected tokenProviderService: TokenProviderService
  ) {
  }

  find(orderID: number): Observable<EntityResponseType> {
    let headers = new HttpHeaders();
    headers = headers.set(JWT_CLIENT_HEADER, this.tokenProviderService.generateToken());
    return this.http.get<IBoondManagerResponse>(`${this.resourceUrl}/${orderID}`, {
      headers: headers,
      observe: 'response'
    });
  }

  getOrderInformation(orderID: number): Observable<EntityResponseType> {
    let headers = new HttpHeaders();
    headers = headers.set(JWT_CLIENT_HEADER, this.tokenProviderService.generateToken());
    return this.http.get<IBoondManagerResponse>(`${this.resourceUrl}/${orderID}/information`, {
      headers: headers,
      observe: 'response'
    });
  }

  getOrderInvoicesInformation(orderID: number): Observable<EntityArrayResponseType> {
    let headers = new HttpHeaders();
    headers = headers.set(JWT_CLIENT_HEADER, this.tokenProviderService.generateToken());
    return this.http.get<IBoondManagerArrayResponse>(`${this.resourceUrl}/${orderID}/invoices`, {
      headers: headers,
      observe: 'response'
    });
  }


}
