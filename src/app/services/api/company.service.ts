import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams, HttpResponse} from "@angular/common/http";
import {API_MAX_RESULTS_PARAM, COMPANY_API_STATES, JWT_CLIENT_HEADER, SERVER_API_URL} from "../../app.constants";
import {Observable} from "rxjs";
import {TokenProviderService} from "../token/token-provider.service";
import {IBoondManagerResponse} from "../../models/api/boondmanager-response.model";
import {IBoondManagerArrayResponse} from "../../models/api/boondmanager-array-response.model";

type EntityResponseType = HttpResponse<IBoondManagerResponse>;
type EntityArrayResponseType = HttpResponse<IBoondManagerArrayResponse>;

@Injectable({
  providedIn: 'root'
})
export class CompanyService {

  public resourceUrl = SERVER_API_URL + 'api/companies';

  constructor(
    protected http: HttpClient,
    protected tokenProviderService: TokenProviderService
  ) {
  }

  find(companyID: number): Observable<EntityResponseType> {
    let headers = new HttpHeaders();
    headers = headers.set(JWT_CLIENT_HEADER, this.tokenProviderService.generateToken());
    return this.http.get<IBoondManagerResponse>(`${this.resourceUrl}/${companyID}`, {
      headers: headers,
      observe: 'response'
    });
  }

  getAllClientCompanies(): Observable<EntityArrayResponseType> {
    let headers = new HttpHeaders();
    headers = headers.set(JWT_CLIENT_HEADER, this.tokenProviderService.generateToken());
    let params = new HttpParams();
    params = params.set(COMPANY_API_STATES, '0,1,7');
    params = params.set(API_MAX_RESULTS_PARAM, 500);
    return this.http.get<IBoondManagerArrayResponse>(`${this.resourceUrl}`, {
      headers: headers,
      params: params,
      observe: 'response'
    });
  }

  getCompanyInvoicesInformation(companyID: number): Observable<EntityArrayResponseType> {
    let headers = new HttpHeaders();
    headers = headers.set(JWT_CLIENT_HEADER, this.tokenProviderService.generateToken());
    return this.http.get<IBoondManagerArrayResponse>(`${this.resourceUrl}/${companyID}/invoices`, {
      headers: headers,
      observe: 'response'
    });
  }

  getCompanyProjectsInformation(companyID: number): Observable<EntityArrayResponseType> {
    let headers = new HttpHeaders();
    headers = headers.set(JWT_CLIENT_HEADER, this.tokenProviderService.generateToken());
    return this.http.get<IBoondManagerArrayResponse>(`${this.resourceUrl}/${companyID}/projects`, {
      headers: headers,
      observe: 'response'
    });
  }

}
