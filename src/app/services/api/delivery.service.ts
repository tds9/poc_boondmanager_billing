import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpResponse} from "@angular/common/http";
import {JWT_CLIENT_HEADER, SERVER_API_URL} from "../../app.constants";
import {Observable} from "rxjs";
import {TokenProviderService} from "../token/token-provider.service";
import {IBoondManagerResponse} from "../../models/api/boondmanager-response.model";
import {IBoondManagerArrayResponse} from "../../models/api/boondmanager-array-response.model";

type EntityResponseType = HttpResponse<IBoondManagerResponse>;
type EntityArrayResponseType = HttpResponse<IBoondManagerArrayResponse>;

@Injectable({
  providedIn: 'root'
})
export class DeliveryService {

  public resourceUrl = SERVER_API_URL + 'api/deliveries';

  constructor(
    protected http: HttpClient,
    protected tokenProviderService: TokenProviderService
  ) {
  }

  find(deliveryID: number): Observable<EntityResponseType> {
    let headers = new HttpHeaders();
    headers = headers.set(JWT_CLIENT_HEADER, this.tokenProviderService.generateToken());
    return this.http.get<IBoondManagerResponse>(`${this.resourceUrl}/${deliveryID}`, {
      headers: headers,
      observe: 'response'
    });
  }


}
