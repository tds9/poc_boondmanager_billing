import {HttpClient, HttpHeaders, HttpResponse} from "@angular/common/http";
import {IBoondManagerResponse} from "../../models/api/boondmanager-response.model";
import {IBoondManagerArrayResponse} from "../../models/api/boondmanager-array-response.model";
import {Injectable} from "@angular/core";
import {JWT_CLIENT_HEADER, SERVER_API_URL} from "../../app.constants";
import {TokenProviderService} from "../token/token-provider.service";
import {Observable} from "rxjs";


type EntityResponseType = HttpResponse<IBoondManagerResponse>;
type EntityArrayResponseType = HttpResponse<IBoondManagerArrayResponse>;

@Injectable({
  providedIn: 'root'
})
export class TimesReportService {

  public resourceUrl = SERVER_API_URL + 'api/times-reports';

  constructor(
    protected http: HttpClient,
    protected tokenProviderService: TokenProviderService
  ) {
  }

  find(timesReportID: number): Observable<EntityResponseType> {
    let headers = new HttpHeaders();
    headers = headers.set(JWT_CLIENT_HEADER, this.tokenProviderService.generateToken());
    return this.http.get<IBoondManagerResponse>(`${this.resourceUrl}/${timesReportID}`, {
      headers: headers,
      observe: 'response'
    });
  }


}
