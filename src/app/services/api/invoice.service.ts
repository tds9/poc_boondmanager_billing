import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams, HttpResponse} from "@angular/common/http";
import {
  API_MAX_RESULTS_PARAM,
  INVOICE_API_END_DATE_PARAM, INVOICE_API_PERIOD_PARAM,
  INVOICE_API_START_DATE_PARAM,
  JWT_CLIENT_HEADER,
  SERVER_API_URL
} from "../../app.constants";
import {Observable} from "rxjs";
import {TokenProviderService} from "../token/token-provider.service";
import {IBoondManagerResponse} from "../../models/api/boondmanager-response.model";
import {IBoondManagerArrayResponse} from "../../models/api/boondmanager-array-response.model";

type EntityResponseType = HttpResponse<IBoondManagerResponse>;
type EntityArrayResponseType = HttpResponse<IBoondManagerArrayResponse>;

@Injectable({
  providedIn: 'root'
})
export class InvoiceService {

  public resourceUrl = SERVER_API_URL + 'api/invoices';

  constructor(
    protected http: HttpClient,
    protected tokenProviderService: TokenProviderService
  ) {
  }

  find(invoiceID: number): Observable<EntityResponseType> {
    let headers = new HttpHeaders();
    headers = headers.set(JWT_CLIENT_HEADER, this.tokenProviderService.generateToken());
    return this.http.get<IBoondManagerResponse>(`${this.resourceUrl}/${invoiceID}`, {
      headers: headers,
      observe: 'response'
    });
  }

  getInvoiceInformation(invoiceID: number): Observable<EntityResponseType> {
    let headers = new HttpHeaders();
    headers = headers.set(JWT_CLIENT_HEADER, this.tokenProviderService.generateToken());
    return this.http.get<IBoondManagerResponse>(`${this.resourceUrl}/${invoiceID}/information`, {
      headers: headers,
      observe: 'response'
    });
  }

  getInvoicesByStartDateAndEndDate(startDate: string, endDate: string): Observable<EntityArrayResponseType> {
    let headers = new HttpHeaders();
    headers = headers.set(JWT_CLIENT_HEADER, this.tokenProviderService.generateToken());
    let params = new HttpParams();
    params = params.set(INVOICE_API_PERIOD_PARAM, 'period');
    params = params.set(INVOICE_API_START_DATE_PARAM, startDate);
    params = params.set(INVOICE_API_END_DATE_PARAM, endDate);
    params = params.set(API_MAX_RESULTS_PARAM, 500);
    return this.http.get<IBoondManagerArrayResponse>(`${this.resourceUrl}`, {
      headers: headers,
      params: params,
      observe: 'response'
    });
  }


}
