import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams, HttpResponse} from "@angular/common/http";
import {BILLING_API_START_MONTH_PARAM, JWT_CLIENT_HEADER, SERVER_API_URL} from "../../app.constants";
import {Observable} from "rxjs";
import {TokenProviderService} from "../token/token-provider.service";
import {IBoondManagerResponse} from "../../models/api/boondmanager-response.model";

type EntityResponseType = HttpResponse<IBoondManagerResponse>;
type EntityArrayResponseType = HttpResponse<IBoondManagerResponse[]>;

@Injectable({
  providedIn: 'root'
})
export class BillingMonthlyBalanceService {

  public resourceUrl = SERVER_API_URL + 'api/billing-monthly-balance';

  constructor(
    protected http: HttpClient,
    protected tokenProviderService: TokenProviderService
  ) {
  }

  find(stringDate?: string): Observable<EntityResponseType> {
    let params = new HttpParams();
    let headers = new HttpHeaders();
    headers = headers.set(JWT_CLIENT_HEADER, this.tokenProviderService.generateToken());
    if (stringDate) {
      params = params.set(BILLING_API_START_MONTH_PARAM, stringDate);
    }
    return this.http.get<IBoondManagerResponse>(`${this.resourceUrl}`, {
      headers: headers,
      params: params,
      observe: 'response'
    });
  }


}
