import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams, HttpResponse} from "@angular/common/http";
import {
  API_MAX_RESULTS_PARAM,
  INVOICE_API_START_DATE_PARAM,
  JWT_CLIENT_HEADER,
  SERVER_API_URL
} from "../../app.constants";
import {Observable} from "rxjs";
import {TokenProviderService} from "../token/token-provider.service";
import {IBoondManagerResponse} from "../../models/api/boondmanager-response.model";
import {IBoondManagerArrayResponse} from "../../models/api/boondmanager-array-response.model";

type EntityResponseType = HttpResponse<IBoondManagerResponse>;
type EntityArrayResponseType = HttpResponse<IBoondManagerArrayResponse>;

@Injectable({
  providedIn: 'root'
})
export class ResourceService {

  public resourceUrl = SERVER_API_URL + 'api/resources';

  constructor(
    protected http: HttpClient,
    protected tokenProviderService: TokenProviderService
  ) {
  }

  find(resourceID: string): Observable<EntityResponseType> {
    let headers = new HttpHeaders();
    headers = headers.set(JWT_CLIENT_HEADER, this.tokenProviderService.generateToken());
    return this.http.get<IBoondManagerResponse>(`${this.resourceUrl}/${resourceID}`, {
      headers: headers,
      observe: 'response'
    });
  }

  getAllResources(): Observable<EntityArrayResponseType> {
    let headers = new HttpHeaders();
    headers = headers.set(JWT_CLIENT_HEADER, this.tokenProviderService.generateToken());
    let params = new HttpParams();
    params = params.set(API_MAX_RESULTS_PARAM, 500);
    return this.http.get<IBoondManagerArrayResponse>(`${this.resourceUrl}/`, {
      headers: headers,
      params: params,
      observe: 'response'
    });
  }

  getResourceInformation(resourceID: number): Observable<EntityResponseType> {
    let headers = new HttpHeaders();
    headers = headers.set(JWT_CLIENT_HEADER, this.tokenProviderService.generateToken());
    return this.http.get<IBoondManagerResponse>(`${this.resourceUrl}/${resourceID}/information`, {
      headers: headers,
      observe: 'response'
    });
  }

  getResourceProjectsInformation(resourceID: number): Observable<EntityArrayResponseType> {
    let headers = new HttpHeaders();
    headers = headers.set(JWT_CLIENT_HEADER, this.tokenProviderService.generateToken());
    return this.http.get<IBoondManagerArrayResponse>(`${this.resourceUrl}/${resourceID}/projects`, {
      headers: headers,
      observe: 'response'
    });
  }

  getResourceDeliveriesInformation(resourceID: number): Observable<EntityArrayResponseType> {
    let headers = new HttpHeaders();
    headers = headers.set(JWT_CLIENT_HEADER, this.tokenProviderService.generateToken());
    return this.http.get<IBoondManagerArrayResponse>(`${this.resourceUrl}/${resourceID}/deliveries-inactivities`, {
      headers: headers,
      observe: 'response'
    });
  }

  getResourceTimesReportsInformation(resourceID: number): Observable<EntityArrayResponseType> {
    let headers = new HttpHeaders();
    headers = headers.set(JWT_CLIENT_HEADER, this.tokenProviderService.generateToken());
    return this.http.get<IBoondManagerArrayResponse>(`${this.resourceUrl}/${resourceID}/times-reports`, {
      headers: headers,
      observe: 'response'
    });
  }


}
