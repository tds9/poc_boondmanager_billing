import {Injectable} from '@angular/core';
import {ToastrService} from 'ngx-toastr';

@Injectable({providedIn: 'root'})
export class AlertService {

  constructor(
    private toasterService: ToastrService
  ) {
  }

  showProvisionalCustomerInvoiceHtAmountIsLessThanCustomerInvoiceHtAmount(resourceFullName: string) {
    this.toasterService.error(
      `Montant de la facture client HT est inferieur au montant de la facture client prévisionnelle -- Consultant '${resourceFullName}'`,
      'Alerte',
      {
        timeOut: 50000,
        closeButton: true,
        positionClass: 'toast-top-center'
      });
  }

  showCustomerInvoiceAmountIsLessThanProviderInvoiceAmount(resourceFullName: string) {
    this.toasterService.error(
      `Montant de la facture client est inférieur au montant de la facture fournisseur -- Consultant '${resourceFullName}'`,
      'Alerte',
      {
        timeOut: 50000,
        closeButton: true,
        positionClass: 'toast-top-center'
      });
  }

}
