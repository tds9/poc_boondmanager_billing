import {environment} from "../environments/environment";

// KEYS
export const USER_TOKEN = environment.userToken;
export const CLIENT_TOKEN = environment.clientToken;
export const CLIENT_KEY = environment.clientKey;

// UTILS
export const SERVER_API_URL = environment.apiURL;
export const JWT_CLIENT_HEADER = 'X-Jwt-Client-Boondmanager';
export const JWT_APP_HEADER = 'X-Jwt-App-Boondmanager';
export const BILLING_API_START_MONTH_PARAM = 'startMonth';
export const INVOICE_API_START_DATE_PARAM = 'startDate';
export const INVOICE_API_END_DATE_PARAM = 'endDate';
export const INVOICE_API_PERIOD_PARAM = 'period';
export const API_MAX_RESULTS_PARAM = 'maxResults';
export const COMPANY_API_STATES = 'states';
