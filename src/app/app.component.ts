import {Component, OnInit, Renderer2} from '@angular/core';
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  signedRequest!: string;
  iFrameID!: string;
  fromPage!: string;


  constructor(
    private route: ActivatedRoute,
    private renderer: Renderer2
  ) {
  }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
        this.signedRequest = params.signedRequest;
        this.iFrameID = params.iFrameID;
        this.fromPage = params.fromPage;
        if (this.signedRequest && this.iFrameID && !this.fromPage) {
          this.changeAppViewToFixedHeight();
        }
      }
    );
  }

  changeAppViewToFixedHeight() {
    const wrapperDiv = document.querySelector('#app-wrapper-container');
    const mainDiv = document.querySelector('#app-main-container');
    this.renderer.setStyle(wrapperDiv, 'height', '500px');
    this.renderer.setStyle(wrapperDiv, 'overflow', 'auto');
    this.renderer.setStyle(mainDiv, 'height', '100%');
    this.renderer.setStyle(mainDiv, 'overflow', 'revert');
  }

}
