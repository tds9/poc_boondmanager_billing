import {ComponentFixture, TestBed} from '@angular/core/testing';

import {AnnualSummaryDataTableComponent} from './annual-summary-data-table.component';

describe('AnnualSummaryDataTableComponent', () => {
  let component: AnnualSummaryDataTableComponent;
  let fixture: ComponentFixture<AnnualSummaryDataTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AnnualSummaryDataTableComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AnnualSummaryDataTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
