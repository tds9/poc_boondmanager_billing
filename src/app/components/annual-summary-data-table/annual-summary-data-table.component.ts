import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subject, Subscription} from "rxjs";
import * as _moment from "moment";
// tslint:disable-next-line:no-duplicate-imports
// @ts-ignore
import {default as _rollupMoment, Moment} from "moment";
import {AnnualSummaryDataBuilderService} from "../../services/data-builder/annual-summary-data-builder.service";
import {IAnnualSummaryDataTableData} from "../../models/data-table/annual-summary-data-table-data.model";
import {MAT_MOMENT_DATE_ADAPTER_OPTIONS, MomentDateAdapter} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import {FormControl} from "@angular/forms";
import {MatDatepicker} from '@angular/material/datepicker';
import {IMonthlySummaryDataTableData} from "../../models/data-table/monthly-summary-data-table-data.model";
import * as saveAs from 'file-saver';

const moment = _rollupMoment || _moment;

export const YEAR_MODE_FORMATS = {
  parse: {
    dateInput: 'YYYY',
  },
  display: {
    dateInput: 'YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-annual-summary-data-table',
  templateUrl: './annual-summary-data-table.component.html',
  styleUrls: ['./annual-summary-data-table.component.scss'],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS],
    },
    {provide: MAT_DATE_FORMATS, useValue: YEAR_MODE_FORMATS},
  ],
})
export class AnnualSummaryDataTableComponent implements OnInit, OnDestroy {

  readonly Number = Number;

  date = new FormControl(moment());
  maxDate = moment().endOf('year');
  data: IAnnualSummaryDataTableData[] = [];
  selectedData: IAnnualSummaryDataTableData[] = [];
  dataSubscription?: Subscription;

  loading: boolean = false;
  cols!: any[];
  activityValues: number[] = [0, 100];
  exportColumns!: any[];

  constructor(
    private annualSummaryDataBuilderService: AnnualSummaryDataBuilderService) {
  }

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
  }

  ngOnInit(): void {
    this.annualSummaryDataBuilderService.setYear(moment().startOf('year'));


    this.cols = [

      {field: "clientName", header: "Client"},
      // @ts-ignore
      {field: "summary[0].totalMonthInvoicesAmountHT", header: "facturation janvier HT"},
      // @ts-ignore
      {field: "summary[0].totalMonthMarginHT", header: "Marge janvier HT"},
      // // @ts-ignore
      // {field: "summary[1].totalMonthInvoicesAmountHT", header: "Facturation de Février"},
      // // @ts-ignore
      // {field: "summary(1).totalMonthMarginHT", header: "Marge de Février"},
      // {field: "summary(2).totalMonthInvoicesAmountHT", header: "Facturation de Mars"},
      // {field: "summary(2).totalMonthMarginHT", header: "Marge de Mars"},
      // {field: "summary(3).totalMonthInvoicesAmountHT", header: "Facturation de Avril"},
      // {field: "summary(3).totalMonthMarginHT", header: "Marge de Avril"},
      // {field: "summary(4).totalMonthInvoicesAmountHT", header: "Facturation de Mai"},
      // {field: "summary(4).totalMonthMarginHT", header: "Marge de Mai"},
      // {field: "summary(5).totalMonthInvoicesAmountHT", header: "Facturation de Juin"},
      // {field: "summary(5).totalMonthMarginHT", header: "Marge de Juin"},
      // {field: "summary(6).totalMonthInvoicesAmountHT", header: "Facturation de Juillet"},
      // {field: "summary(6).totalMonthMarginHT", header: "Marge de Juillet"},
      // {field: "summary(7).totalMonthInvoicesAmountHT", header: "Facturation de Août"},
      // {field: "summary(7).totalMonthMarginHT", header: "Marge de Août"},
      // {field: "summary(8).totalMonthInvoicesAmountHT", header: "Facturation de Septembre"},
      // {field: "summary(8).totalMonthMarginHT", header: "Marge de Septembre"},
      // {field: "summary(9).totalMonthInvoicesAmountHT", header: "Facturation de Octobre"},
      // {field: "summary(9).totalMonthMarginHT", header: "Marge de Octobre"},
      // {field: "summary(10).totalMonthInvoicesAmountHT", header: "Facturation de Novembre"},
      // {field: "summary(10).totalMonthMarginHT", header: "Marge de Novembre"},
      // {field: "summary(11).totalMonthInvoicesAmountHT", header: "Facturation de Décembre"},
      // {field: "summary(11).totalMonthMarginHT", header: "Marge de Décembre"},
      {field: "totalYearInvoicesAmountTTC", header: "facturation total HT"},
      {field: "totalDeltaYearHT", header: "Marge Total HT"},
      {field: "percentageTotalDeltaYearHT", header: "% Marge"},
    ];
    this.exportColumns = this.cols.map(col => ({
      title: col.header,
      dataKey: col.field
    }));

    this.dataSubscription = this.annualSummaryDataBuilderService.getData().subscribe(value => {
      this.data = value;
      console.log('---------- annual data');
      console.log(this.data);
    });
    this.annualSummaryDataBuilderService.buildDataTableData();
  }

  chosenYearHandler(normalizedYear: Moment, datepicker: MatDatepicker<Moment>) {
    const ctrlValue = this.date.value;
    ctrlValue.year(normalizedYear.year());
    this.date.setValue(ctrlValue);
    datepicker.close();
    this.annualSummaryDataBuilderService.setYear(this.date.value.startOf('year'), true);
  }

  applyFilterGlobal($event: any, stringVal: string, dt: any) {
    dt.filterGlobal(($event.target as HTMLInputElement).value, stringVal);
  }

  exportPdf() {
    const exceData: any[] = this.data.map(item => {
      return {

        "Client": item.clientName,
        // @ts-ignore
        "facturation janvier HT": item.summary[0].totalMonthInvoicesAmountHT,
        // @ts-ignore
        "Marge janvier HT": item.summary[0].totalMonthMarginHT,
        // @ts-ignore
        "facturation Février HT": item.summary[1].totalMonthInvoicesAmountHT,
        // @ts-ignore
        "Marge Février HT": item.summary[1].totalMonthMarginHT,
        // @ts-ignore
        "facturation Mars HT": item.summary[2].totalMonthInvoicesAmountHT,
        // @ts-ignore
        "Marge Mars HT": item.summary[2].totalMonthMarginHT,
        // @ts-ignore
        "facturation Avril HT": item.summary[3].totalMonthInvoicesAmountHT,
        // @ts-ignore
        "Marge Avril HT": item.summary[3].totalMonthMarginHT,
        // @ts-ignore
        "facturation Mai HT": item.summary[4].totalMonthInvoicesAmountHT,
        // @ts-ignore
        "Marge Mai HT": item.summary[4].totalMonthMarginHT,
        // @ts-ignore
        "facturation Juin HT": item.summary[5].totalMonthInvoicesAmountHT,
        // @ts-ignore
        "Marge Juin HT": item.summary[5].totalMonthMarginHT,
        // @ts-ignore
        "facturation Juillet HT": item.summary[6].totalMonthInvoicesAmountHT,
        // @ts-ignore
        "Marge Juillet HT": item.summary[6].totalMonthMarginHT,
        // @ts-ignore
        "facturation Août HT": item.summary[7].totalMonthInvoicesAmountHT,
        // @ts-ignore
        "Marge Août HT": item.summary[7].totalMonthMarginHT,
        // @ts-ignore
        "facturation Septembre HT": item.summary[8].totalMonthInvoicesAmountHT,
        // @ts-ignore
        "Marge Septembre HT": item.summary[8].totalMonthMarginHT,
        // @ts-ignore
        "facturation Octobre HT": item.summary[9].totalMonthInvoicesAmountHT,
        // @ts-ignore
        "Marge Octobre HT": item.summary[9].totalMonthMarginHT,
        // @ts-ignore
        "facturation Novembre HT": item.summary[10].totalMonthInvoicesAmountHT,
        // @ts-ignore
        "Marge Novembre HT": item.summary[10].totalMonthMarginHT,
        // @ts-ignore
        "facturation Décembre HT": item.summary[11].totalMonthInvoicesAmountHT,
        // @ts-ignore
        "Marge Décembre HT": item.summary[11].totalMonthMarginHT,

        "facturation total HT": item.totalYearInvoicesAmountHT,
        "Marge Total HT": item.totalYearMarginHT,
        "% Marge": Number((item.percentageTotalYearMarginHT!).toFixed(2)) + ' %'


      }
    })

    import("jspdf").then(jsPDF => {
      import("jspdf-autotable").then(x => {
        let doc = new jsPDF.default('l', 'pt', 'a4');
        doc.setFontSize(12);
        doc.text(`Synthese de la production annuelle ${this.date.value.format('YYYY')}`, 30, 30);


        (doc as any).autoTable(this.exportColumns, this.data);
        doc.save(`Synthese_Production_annuelle_${this.date.value.format('YYYY')}.pdf`);

      })
    })
  }


  exportExcel() {

    const exceData: any[] = this.data.map(item => {
      return {

        "Client": item.clientName,
        // @ts-ignore
        "facturation janvier HT": item.summary[0].totalMonthInvoicesAmountHT,
        // @ts-ignore
        "Marge janvier HT": item.summary[0].totalMonthMarginHT,
        // @ts-ignore
        "facturation Février HT": item.summary[1].totalMonthInvoicesAmountHT,
        // @ts-ignore
        "Marge Février HT": item.summary[1].totalMonthMarginHT,
        // @ts-ignore
        "facturation Mars HT": item.summary[2].totalMonthInvoicesAmountHT,
        // @ts-ignore
        "Marge Mars HT": item.summary[2].totalMonthMarginHT,
        // @ts-ignore
        "facturation Avril HT": item.summary[3].totalMonthInvoicesAmountHT,
        // @ts-ignore
        "Marge Avril HT": item.summary[3].totalMonthMarginHT,
        // @ts-ignore
        "facturation Mai HT": item.summary[4].totalMonthInvoicesAmountHT,
        // @ts-ignore
        "Marge Mai HT": item.summary[4].totalMonthMarginHT,
        // @ts-ignore
        "facturation Juin HT": item.summary[5].totalMonthInvoicesAmountHT,
        // @ts-ignore
        "Marge Juin HT": item.summary[5].totalMonthMarginHT,
        // @ts-ignore
        "facturation Juillet HT": item.summary[6].totalMonthInvoicesAmountHT,
        // @ts-ignore
        "Marge Juillet HT": item.summary[6].totalMonthMarginHT,
        // @ts-ignore
        "facturation Août HT": item.summary[7].totalMonthInvoicesAmountHT,
        // @ts-ignore
        "Marge Août HT": item.summary[7].totalMonthMarginHT,
        // @ts-ignore
        "facturation Septembre HT": item.summary[8].totalMonthInvoicesAmountHT,
        // @ts-ignore
        "Marge Septembre HT": item.summary[8].totalMonthMarginHT,
        // @ts-ignore
        "facturation Octobre HT": item.summary[9].totalMonthInvoicesAmountHT,
        // @ts-ignore
        "Marge Octobre HT": item.summary[9].totalMonthMarginHT,
        // @ts-ignore
        "facturation Novembre HT": item.summary[10].totalMonthInvoicesAmountHT,
        // @ts-ignore
        "Marge Novembre HT": item.summary[10].totalMonthMarginHT,
        // @ts-ignore
        "facturation Décembre HT": item.summary[11].totalMonthInvoicesAmountHT,
        // @ts-ignore
        "Marge Décembre HT": item.summary[11].totalMonthMarginHT,

        "facturation total HT": item.totalYearInvoicesAmountHT,
        "Marge Total HT": item.totalYearMarginHT,
        "% Marge ": Number((item.percentageTotalYearMarginHT!).toFixed(2)) + ' %'


      }
    })
    import("xlsx").then(xlsx => {

      const worksheet = xlsx.utils.json_to_sheet(exceData);
      const workbook = {Sheets: {data: worksheet}, SheetNames: ["data"]};

      const excelBuffer: any = xlsx.write(workbook, {
        bookType: "xlsx",
        type: "array"
      });
      this.saveAsExcelFile(excelBuffer, "Synthese_Production_Annuelle");
    });
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    import("file-saver").then(FileSaver => {
      let EXCEL_TYPE =
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8";
      let EXCEL_EXTENSION = ".xlsx";
      const data: Blob = new Blob([buffer], {
        type: EXCEL_TYPE
      });
      saveAs(
        data,
        fileName + `${this.date.value.format('_YYYY')}` + EXCEL_EXTENSION
      );
    });
  }
}
