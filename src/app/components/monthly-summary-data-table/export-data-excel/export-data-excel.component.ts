import {Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges} from '@angular/core';
import {Moment} from 'moment';
import * as moment from 'moment';
import {IMonthlySummaryDataTableData} from "../../../models/data-table/monthly-summary-data-table-data.model";
import {Subscription} from "rxjs";
import {MonthlySummaryDataBuilderService} from "../../../services/data-builder/monthly-summary-data-builder.service";
import {IMonthlySummaryTotalsAmountsData} from "../../../models/data-table/monthly-summary-totals-amounts-data.model";
import {NgxSpinnerService} from "ngx-spinner";
import {saveAs} from 'file-saver';
import 'jspdf-autotable';

@Component({
  selector: 'app-export-data-excel',
  templateUrl: './export-data-excel.component.html',
  styleUrls: ['./export-data-excel.component.scss'],
})
export class ExportDataExcelComponent implements OnDestroy, OnInit, OnChanges {

  @Output() reloadDataAfterExcelExportationEvent = new EventEmitter<Date>();
  @Input() startMonth!: Date;

  endMonth!: Date;
  startMonthTmp!: Date;

  startMonthMinDate!: Date;
  startMonthMaxDate!: Date;
  endMonthMinDate!: Date;
  endMonthMaxDate!: Date;

  dataSubscription?: Subscription;

  monthsToExport: Moment[] = [];
  dataToExport: any[] = [];

  constructor(
    private spinner: NgxSpinnerService,
    private monthlySummaryDataBuilderService: MonthlySummaryDataBuilderService
  ) {
  }

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
  }

  ngOnInit(): void {
    this.endMonth = this.startMonth;
    this.startMonthTmp = this.startMonth;
    this.startMonthMinDate = moment('2020-03-01').startOf('month').toDate();
    this.startMonthMaxDate = moment().endOf('month').toDate();
    this.endMonthMinDate = this.startMonth;
    this.endMonthMaxDate = moment().endOf('month').toDate();
    this.monthsToExport = [];
    this.dataToExport = [];

    this.dataSubscription = this.monthlySummaryDataBuilderService.getData().subscribe((value: { totalsAmountsData: IMonthlySummaryTotalsAmountsData, monthlySummaryDataTableData: IMonthlySummaryDataTableData[], month: Moment }) => {
      let data: IMonthlySummaryDataTableData[] = value.monthlySummaryDataTableData;
      let month: Moment = value.month.startOf('month');

      if (this.monthsToExport.includes(month)) {
        data.forEach(item => {
          this.dataToExport.push({
            "Mois": month.format('MM-YYYY'),
            "Consultant": item.consultantFullName,
            "Client": item.finalCustomerName ? item.finalCustomerName! : '-',
            "Salarié/Indépendant": item.isEmployee ? 'S' : 'I',
            "Commercial": item.commercialFullName ? item.commercialFullName : '-',
            "Production du mois": item.monthProduction! ? item.monthProduction! : '-',
            "Nbre jours restant": item.orderRemainingDays! ? item.orderRemainingDays! : '-',
            "Facturation Previsionnelle client H.T": item.forecastCustomerBillingHT! ? item.forecastCustomerBillingHT! + '€' : '-',
            "Facturation client H.T": item.customerInvoiceReceivedHT! ? item.customerInvoiceReceivedHT! + ' €' : '-',
            "Facturation client TTC": item.customerInvoiceReceivedTTC! ? item.customerInvoiceReceivedTTC! + ' €' : '-',
            "Date Paiement client reçu": item.customerPayment! ? item.customerPayment! : '-',
            "Facture prévisionnelle fournisseur H.T": item.provisionalInvoicingSupplierHT! ? item.provisionalInvoicingSupplierHT! + ' €' : '-',
            "Facture fournisseur reçu H.T": item.supplierInvoiceReceivedHT! ? item.supplierInvoiceReceivedHT! + ' €' : '-',
            "Facture fournisseur reçu TTC": item.supplierInvoiceReceivedTTC! ? item.supplierInvoiceReceivedTTC! + ' €' : '-',
            "Date Paiement fournisseur": item.supplierPayment! ? item.supplierPayment! : '-',
            "Marge": item.margin! ? (item.margin!).toFixed(2).replace('.', ',') + ' €' : '-'
          });
        });
        let index = this.monthsToExport.indexOf(month);
        this.monthsToExport.splice(index, 1);
      }

      if (this.monthsToExport.length > 0) {
        this.monthlySummaryDataBuilderService.setMonth(this.monthsToExport[0], true);
      } else if (this.monthsToExport.length === 0 && this.dataToExport.length > 0) {
        this.spinner.hide();
        this.exportExcel();
      }

    });

  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.startMonth) {
      this.endMonth = changes.startMonth.currentValue;
      this.startMonthTmp = changes.startMonth.currentValue;
      this.endMonthMinDate = this.startMonth;
      this.monthsToExport = [];
      this.dataToExport = [];
    }
  }

  handleStartMonthSelect(): void {
    if (moment(this.endMonth).isBefore(moment(this.startMonth))) {
      this.endMonth = this.startMonth;
    }
    this.endMonthMinDate = this.startMonth;
  }

  startExcelFileExportation(): void {

    console.log('Generate Excel File');

    this.monthsToExport = [];
    this.dataToExport = [];

    let monthStart: Date = this.startMonth;
    let monthEnd: Date = this.endMonth;

    while (moment(monthEnd).isAfter(moment(monthStart)) || moment(monthEnd).isSame(moment(monthStart), 'month')) {
      this.monthsToExport.push(moment(monthStart));
      monthStart = moment(monthStart).clone().add(1, 'months').toDate();
    }

    this.monthsToExport = this.monthsToExport.map(date => date.startOf('month'));

    this.spinner.show();
    this.monthlySummaryDataBuilderService.setMonth(this.monthsToExport[0], true);

  }

  private exportExcel(): void {
    import("xlsx").then(xlsx => {
      var worksheet = xlsx.utils.json_to_sheet(this.dataToExport);
      // xlsx.utils.sheet_add_aoa(worksheet, [["Production Mensuelle : " + `${this.date.value.format('MM_YYYY')}`]], {origin:"A1"});
      // xlsx.utils.sheet_add_aoa(worksheet, [[""]], {origin: -1});
      // xlsx.utils.sheet_add_aoa(worksheet, [["Total Production Mensuelle : " + `${this.date.value.format('MM_YYYY')}`, , , , , "Total :", this.totalsAmounts.forecastCustomerBillingHTTotal + ' €', this.totalsAmounts.customerInvoiceReceivedHTTotal + ' €', this.totalsAmounts.customerInvoiceReceivedTTCTotal + ' €', , this.totalsAmounts.provisionalInvoicingSupplierHTTotal + ' €', this.totalsAmounts.supplierInvoiceReceivedHTTotal + ' €', this.totalsAmounts.supplierInvoiceReceivedTTCTotal + ' €', , this.totalsAmounts.marginTotal + ' €']], {origin: -1});
      const workbook = {Sheets: {data: worksheet}, SheetNames: ["data"]};
      const excelBuffer: any = xlsx.write(workbook, {
        bookType: "xlsx",
        type: "array"
      });
      this.saveAsExcelFile(excelBuffer, "Synthese_Production_Mensuelle");
    });
  }

  private saveAsExcelFile(buffer: any, fileName: string): void {
    import("file-saver").then(FileSaver => {
      const EXCEL_TYPE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8";
      const EXCEL_EXTENSION = ".xlsx";
      const data: Blob = new Blob([buffer], {
        type: EXCEL_TYPE
      });
      saveAs(data, fileName + EXCEL_EXTENSION);
      // handle the component
      this.monthsToExport = [];
      this.dataToExport = [];
      this.reloadDataAfterExcelExportationEvent.emit(this.startMonthTmp);
    });
  }

}
