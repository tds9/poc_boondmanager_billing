import {ComponentFixture, TestBed} from '@angular/core/testing';

import {MonthlySummaryDataTableComponent} from './monthly-summary-data-table.component';

describe('DataTableComponent', () => {
  let component: MonthlySummaryDataTableComponent;
  let fixture: ComponentFixture<MonthlySummaryDataTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MonthlySummaryDataTableComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MonthlySummaryDataTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
