import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {Subscription} from "rxjs";
import {MonthlySummaryDataBuilderService} from "../../services/data-builder/monthly-summary-data-builder.service";
import {IMonthlySummaryDataTableData} from "../../models/data-table/monthly-summary-data-table-data.model";
import * as _moment from 'moment';
// tslint:disable-next-line:no-duplicate-imports
// @ts-ignore
import {default as _rollupMoment, Moment} from 'moment';
import {MAT_MOMENT_DATE_ADAPTER_OPTIONS, MomentDateAdapter} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import {FormControl} from "@angular/forms";
import {MatDatepicker} from '@angular/material/datepicker';
import {saveAs} from 'file-saver';
import 'jspdf-autotable';
import {IMonthlySummaryTotalsAmountsData} from "../../models/data-table/monthly-summary-totals-amounts-data.model";
import {NgxSpinnerService} from "ngx-spinner";

const moment = _rollupMoment || _moment;

export const MY_FORMATS = {
  parse: {
    dateInput: 'MM/YYYY',
  },
  display: {
    dateInput: 'MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-monthly-summary-data-table',
  templateUrl: './monthly-summary-data-table.component.html',
  styleUrls: ['./monthly-summary-data-table.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS],
    },
    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ],
})
export class MonthlySummaryDataTableComponent implements OnDestroy, OnInit {

  readonly Number = Number;

  selectedMonth: Moment = moment().startOf('month').subtract('1', 'months').startOf('month');
  selectedMonthDateFormat: Date = this.selectedMonth.toDate();

  date = new FormControl(moment().subtract('1', 'months'));
  maxDate = moment().endOf('month');
  minDate = moment('2020-03-01').startOf('month');
  totalsAmounts!: IMonthlySummaryTotalsAmountsData;
  data: IMonthlySummaryDataTableData[] = [];
  dataSubscription?: Subscription;


  loading: boolean = false;

  cols!: any[];
  // exportColumns!: any[];

  display = false;

  constructor(
    private spinner: NgxSpinnerService,
    private monthlySummaryDataBuilderService: MonthlySummaryDataBuilderService
  ) {
  }

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
  }

  ngOnInit(): void {

    this.monthlySummaryDataBuilderService.setMonth(this.selectedMonth);

    this.cols = [
      {field: "consultantFullName", header: "Consultant"},
      {field: "finalCustomerName", header: "Client"},
      {field: "isEmployee", header: "Salarié / Indépendant"},
      {field: "commercialFullName", header: "Commercial TDS"},
      {field: "monthProduction", header: "Production du mois"},
      {field: "orderRemainingDays", header: "Nbre Jours Restant"},
      {field: "forecastCustomerBillingHT", header: "Facturation Previsionnelle client H.T"},
      {field: "customerInvoiceReceivedHT", header: "Facturation client H.T"},
      {field: "customerInvoiceReceivedTTC", header: "Facturation client TTC"},
      {field: "customerPayment", header: "Date Paiement client reçu"},
      {field: "provisionalInvoicingSupplierHT", header: "Facture prévisionnelle fournisseur H.T"},
      {field: "supplierInvoiceReceivedHT", header: "Facture fournisseur reçu H.T"},
      {field: "supplierInvoiceReceivedTTC", header: "Facture fournisseur reçu TTC"},
      {field: "supplierPayment", header: "Date Paiement fournisseur"},
      {field: "margin", header: "Marge"},
    ];

    this.dataSubscription = this.monthlySummaryDataBuilderService.getData().subscribe((value: { totalsAmountsData: IMonthlySummaryTotalsAmountsData, monthlySummaryDataTableData: IMonthlySummaryDataTableData[], month: Moment }) => {
      console.log(value);
      this.totalsAmounts = value.totalsAmountsData;
      this.data = value.monthlySummaryDataTableData;
      if (!this.display) this.spinner.hide();
    });
    this.spinner.show();
    this.monthlySummaryDataBuilderService.buildDataTableData();
  }

  chosenYearHandler(normalizedYear: Moment) {
    const ctrlValue = this.date.value;
    ctrlValue.year(normalizedYear.year());
    this.date.setValue(ctrlValue);
  }

  chosenMonthHandler(normalizedMonth: Moment, datepicker: MatDatepicker<Moment>) {
    const ctrlValue = this.date.value;
    ctrlValue.month(normalizedMonth.month());
    this.date.setValue(ctrlValue);
    datepicker.close();
    this.selectedMonth = this.date.value.startOf('month');
    this.selectedMonthDateFormat = this.selectedMonth.toDate();
    this.spinner.show();
    this.monthlySummaryDataBuilderService.setMonth(this.selectedMonth, true);
  }

  applyFilterGlobal($event: any, stringVal: string, dt: any) {
    dt.filterGlobal(($event.target as HTMLInputElement).value, stringVal);
  }

  exportExcel() {

    const exceData: any[] = this.data.map(item => {
      return {
        "Consultant": item.consultantFullName,
        "Client": item.finalCustomerName ? item.finalCustomerName! : '-',
        "Salarié/Indépendant": item.isEmployee ? 'S' : 'I',
        "Commercial": item.commercialFullName ? item.commercialFullName : '-',
        "Production du mois": item.monthProduction! ? item.monthProduction! : '-',
        "Nbre jours restant": item.orderRemainingDays! ? item.orderRemainingDays! : '-',
        "Facturation Previsionnelle client H.T": item.forecastCustomerBillingHT! ? item.forecastCustomerBillingHT! + '€' : '-',
        "Facturation client H.T": item.customerInvoiceReceivedHT! ? item.customerInvoiceReceivedHT! + ' €' : '-',
        "Facturation client TTC": item.customerInvoiceReceivedTTC! ? item.customerInvoiceReceivedTTC! + ' €' : '-',
        "Date Paiement client reçu": item.customerPayment! ? item.customerPayment! : '-',
        "Facture prévisionnelle fournisseur H.T": item.provisionalInvoicingSupplierHT! ? item.provisionalInvoicingSupplierHT! + ' €' : '-',
        "Facture fournisseur reçu H.T": item.supplierInvoiceReceivedHT! ? item.supplierInvoiceReceivedHT! + ' €' : '-',
        "Facture fournisseur reçu TTC": item.supplierInvoiceReceivedTTC! ? item.supplierInvoiceReceivedTTC! + ' €' : '-',
        "Date Paiement fournisseur": item.supplierPayment! ? item.supplierPayment! : '-',
        "Marge": item.margin! ? (item.margin!).toFixed(2).replace('.', ',') + ' €' : '-'
      }
    });

    import("xlsx").then(xlsx => {

      var worksheet = xlsx.utils.json_to_sheet(exceData);
      // xlsx.utils.sheet_add_aoa(worksheet, [["Production Mensuelle : " + `${this.date.value.format('MM_YYYY')}`]], {origin:"A1"});
      // xlsx.utils.sheet_add_aoa(worksheet, [[""]], {origin: -1});
      // xlsx.utils.sheet_add_aoa(worksheet, [["Total Production Mensuelle : " + `${this.date.value.format('MM_YYYY')}`, , , , , "Total :", this.totalsAmounts.forecastCustomerBillingHTTotal + ' €', this.totalsAmounts.customerInvoiceReceivedHTTotal + ' €', this.totalsAmounts.customerInvoiceReceivedTTCTotal + ' €', , this.totalsAmounts.provisionalInvoicingSupplierHTTotal + ' €', this.totalsAmounts.supplierInvoiceReceivedHTTotal + ' €', this.totalsAmounts.supplierInvoiceReceivedTTCTotal + ' €', , this.totalsAmounts.marginTotal + ' €']], {origin: -1});
      const workbook = {Sheets: {data: worksheet}, SheetNames: ["data"]};

      const excelBuffer: any = xlsx.write(workbook, {
        bookType: "xlsx",
        type: "array"
      });
      this.saveAsExcelFile(excelBuffer, "Synthese_Production_Mensuelle");
    });

  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    import("file-saver").then(FileSaver => {
      let EXCEL_TYPE =
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8";
      let EXCEL_EXTENSION = ".xlsx";
      const data: Blob = new Blob([buffer], {
        type: EXCEL_TYPE
      });
      saveAs(
        data,
        fileName + "_" + `${this.date.value.format('MM_YYYY')}` + EXCEL_EXTENSION
      );
    });
  }

  checkRowError_forecastCustomer(dataRow: IMonthlySummaryDataTableData): boolean {
    return (dataRow.customerInvoiceReceivedHT! < dataRow.forecastCustomerBillingHT!);
  }

  checkRowError_supplierInvoice(dataRow: IMonthlySummaryDataTableData): boolean {
    return (dataRow.customerInvoiceReceivedHT! < dataRow.supplierInvoiceReceivedHT!);
  }

  handleReloadDataAfterExcelExportationEvent(date: Date): void {
    this.display = false;
    this.selectedMonth = moment(date).startOf('month');
    this.selectedMonthDateFormat = this.selectedMonth.toDate();
    this.spinner.show();
    this.monthlySummaryDataBuilderService.setMonth(this.selectedMonth, true);
  }

}
