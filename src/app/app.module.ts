import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {
  MonthlySummaryDataTableComponent
} from './components/monthly-summary-data-table/monthly-summary-data-table.component';
import {
  AnnualSummaryDataTableComponent
} from './components/annual-summary-data-table/annual-summary-data-table.component';
import {
  ExportDataExcelComponent
} from "./components/monthly-summary-data-table/export-data-excel/export-data-excel.component";
import {DataTablesModule} from "angular-datatables";
import {SidebarComponent} from "./layouts/sidebar/sidebar.component";
import {FooterComponent} from "./layouts/footer/footer.component";
import {NavbarComponent} from "./layouts/navbar/navbar.component";
import {NgxSpinnerModule} from "ngx-spinner";
import {NgxSpinnerComponent} from "./components/ngx-spinner/ngx-spinner.component";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatInputModule} from '@angular/material/input';
import {ToastrModule} from 'ngx-toastr';
import {TableModule} from 'primeng/table';
import {ButtonModule} from 'primeng/button';
import {InputTextModule} from 'primeng/inputtext';
import {StyleClassModule} from 'primeng/styleclass';
import {TooltipModule} from 'primeng/tooltip';
import {SidebarModule} from 'primeng/sidebar';
import {CalendarModule} from 'primeng/calendar';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';


@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule,
    DataTablesModule,
    NgxSpinnerModule,
    ToastrModule.forRoot(),
    MatFormFieldModule,
    MatDatepickerModule,
    MatInputModule,
    TableModule,
    ButtonModule,
    InputTextModule,
    StyleClassModule,
    TooltipModule,
    SidebarModule,
    CalendarModule,
    MatButtonModule,
    MatIconModule
  ],
  declarations: [
    AppComponent,
    NgxSpinnerComponent,
    SidebarComponent,
    FooterComponent,
    NavbarComponent,
    MonthlySummaryDataTableComponent,
    AnnualSummaryDataTableComponent,
    ExportDataExcelComponent
  ],
  providers: [],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class AppModule {
}
