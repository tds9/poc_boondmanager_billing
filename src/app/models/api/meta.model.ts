export interface IMetaTotals {
  rows?: number;
  turnoverInvoicedExcludingTax?: number;
  turnoverProductionExcludingTax?: number;
  deltaProductionExcludingTax?: number;
}

export interface IMeta {
  version?: string;
  isLogged?: boolean;
  language?: string;
  totals?: IMetaTotals;
}

export class Meta implements IMeta {
  constructor(
    public version?: string,
    public isLogged?: boolean,
    public language?: string,
    public totals?: IMetaTotals
  ) {
  }
}
