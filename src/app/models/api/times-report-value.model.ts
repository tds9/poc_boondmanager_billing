export enum TimesReportValue {
  SAVEDANDNOVALIDATION = 'savedAndNoValidation',
  VALIDATED = 'validated',
  REJECTED = 'rejected',
}
