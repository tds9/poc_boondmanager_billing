import {IMeta} from "./meta.model";
import {IData} from "./utils.models";

export interface IBoondManagerArrayResponse {
  meta?: IMeta;
  name?: string;
  data?: IData[];
  included?: IData[];
}

export class BoondManagerArrayResponse implements IBoondManagerArrayResponse {
  constructor(
    public meta?: IMeta,
    public name?: string,
    public data?: IData[],
    public included?: IData[]
  ) {
  }
}
