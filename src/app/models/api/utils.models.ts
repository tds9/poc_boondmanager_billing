import {DataType} from "./data-type.model";
import {TimesReportValue} from "./times-report-value.model";
import {TimesReportWorkUnitTimeType} from "./times-report-work-unit-time-type";

export interface IData {
  id?: number;
  type?: DataType;
  attributes?: IAttribute;
  relationships?: IRelationShips;
}

export interface IRelationShip {
  data?: IData;
}

export interface IRelationShips {
  mainManager?: IRelationShip;
  agency?: IRelationShip;
  company?: IRelationShip;
  project?: IRelationShip;
  opportunity?: IRelationShip;
  contact?: IRelationShip;
  pole?: IRelationShip;
  purchase?: IRelationShip;
  order?: IRelationShip;
}

export interface IAttribute {
  // common project, billing_monthly_balance
  reference?: string;
  // for project
  typeOf?: number;
  mode?: number;
  creationDate?: Date;
  updateDate?: Date;
  isProjectManager?: boolean;
  currencyAgency?: number;
  currency?: number;
  exchangeRate?: number;
  exchangeRateAgency?: number;
  // for billing_monthly_balance
  date?: string;
  number?: string;
  numberOfInvoices?: number;
  turnoverProductionExcludingTax?: number;
  turnoverInvoicedExcludingTax?: number;
  deltaProductionExcludingTax?: number;
  separateActivityExpensesAndPurchases?: boolean;
  // others
  name?: string;
  allowAdvantagesOnProjects?: boolean;
  firstName?: string;
  lastName?: string;
  title?: string;
  // for times-report
  absencesTimes?: ITimesreportUnitTime[];
  regularTimes?: ITimesreportUnitTime[];
  // for delivery
  startDate?: string;
  endDate?: string;
  averageDailyPriceExcludingTax?: number;
  averageDailyCost?: number;
  state?: TimesReportValue | number;
  term?: string;
  numberOfDaysInvoicedOrQuantity?: number;
  // for payment
  amountExcludingTax?: number;
  amountIncludingTax?: number;
  performedDate?: string;
  // for invoice
  totalExcludingTax?: number;
  totalIncludingTax?: number;
  performedPaymentDate?: string;
  expectedPaymentDate?: string;
  activityDetails?: IInvoiceActivityDetailsItem[];
  // for order
  turnoverOrderedExcludingTax?: number;
}

export interface IInvoiceActivityDetailsItem {
  resource?: IInvoiceActivityDetailsItemResource;
}

export interface IInvoiceActivityDetailsItemResource {
  id?: number;
  firstName?: string;
  lastName?: string;
  canReadResource?: boolean;
  exceptionalCalendars?: any[];
  exceptionalTimes?: any[];
  expensesReports?: any[];
  regularTimes?: any[];
  timesReports?: any[];
}

export interface ITimesreportUnitTime {
  id?: number;
  duration?: number;
  workUnitType?: IWorkUnitTypeTimesreportUnitTime;
}

export interface IWorkUnitTypeTimesreportUnitTime {
  activityType?: TimesReportWorkUnitTimeType;
}
