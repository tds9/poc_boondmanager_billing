import {IMeta} from "./meta.model";
import {IData} from "./utils.models";

export interface IBoondManagerResponse {
  meta?: IMeta;
  name?: string;
  data?: IData;
  included?: IData[];
}

export class BoondManagerResponse implements IBoondManagerResponse {
  constructor(
    public meta?: IMeta,
    public name?: string,
    public data?: IData,
    public included?: IData[]
  ) {
  }
}
