export enum DataType {
  COMPANY = 'company',
  OPPORTUNITY = 'opportunity',
  CONTACT = 'contact',
  PROJECT = 'project',
  AGENCY = 'agency',
  RESOURCE = 'resource',
  DELIVERY = 'delivery',
  TIMESREPORT = 'timesreport',
}
