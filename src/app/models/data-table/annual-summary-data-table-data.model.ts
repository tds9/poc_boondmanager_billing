export interface IInvoice {
  invoiceID?: number;
  orderID?: number;
  orderAmountHT?: number;
  invoiceAmountHT?: number;
  invoiceAmountTTC?: number;
  deltaHT?: number;
  startDate?: string;
  endDate?: string;
}

export interface IPayment {
  paymentID?: number;
  startDate?: string;
  endDate?: string;
  paymentAmountHT?: number;
  paymentAmountTTC?: number;
  performedDate?: string;
}

export interface IProject {
  projectID?: number;
  hasPurchases?: boolean;
  purchasesIDs?: number[];
}

export interface IMonthData {
  monthID?: 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12;
  monthName?: string;
  // for client invoices
  invoicesExists?: boolean;
  monthInvoices?: IInvoice[];
  totalMonthInvoicesAmountHT?: number;
  totalMonthInvoicesAmountTTC?: number;
  // for supplier payments
  paymentsExists?: boolean;
  payments?: IPayment[];
  totalMonthPaymentsAmountHT?: number;
  totalMonthPaymentsAmountTTC?: number;
  // for margin
  totalMonthMarginHT?: number;
  totalMonthMarginTTC?: number;
}

export interface IAnnualSummaryDataTableData {
  id?: number;
  clientID?: number;
  clientName?: string;
  summary?: IMonthData[];
  projects?: IProject[];
  invoices?: IInvoice[];
  totalYearInvoicesAmountHT?: number;
  totalYearInvoicesAmountTTC?: number;
  totalYearPaymentsAmountHT?: number;
  totalYearPaymentsAmountTTC?: number;
  totalYearMarginHT?: number;
  totalYearMarginTTC?: number;
  percentageTotalYearMarginHT?: number;
  percentageTotalYearMarginTTC?: number;
}

export class AnnualSummaryDataTableData implements IAnnualSummaryDataTableData {
  constructor(
    public id?: number,
    public clientID?: number,
    public clientName?: string,
    public summary?: IMonthData[],
    public projects?: IProject[],
    public invoices?: IInvoice[],
    public totalYearInvoicesAmountHT?: number,
    public totalYearInvoicesAmountTTC?: number,
    public totalYearPaymentsAmountHT?: number,
    public totalYearPaymentsAmountTTC?: number,
    public totalYearMarginHT?: number,
    public totalYearMarginTTC?: number,
    public percentageTotalYearMarginHT?: number,
    public percentageTotalYearMarginTTC?: number,
  ) {
  }
}
