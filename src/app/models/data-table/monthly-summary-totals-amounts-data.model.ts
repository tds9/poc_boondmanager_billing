export interface IMonthlySummaryTotalsAmountsData {
  forecastCustomerBillingHTTotal: number;
  customerInvoiceReceivedHTTotal: number;
  customerInvoiceReceivedTTCTotal: number;
  provisionalInvoicingSupplierHTTotal: number;
  supplierInvoiceReceivedHTTotal: number;
  supplierInvoiceReceivedTTCTotal: number;
  marginTotal: number;
}

export class MonthlySummaryTotalsAmountsData implements IMonthlySummaryTotalsAmountsData {
  constructor(
    public forecastCustomerBillingHTTotal: number,
    public customerInvoiceReceivedHTTotal: number,
    public customerInvoiceReceivedTTCTotal: number,
    public provisionalInvoicingSupplierHTTotal: number,
    public supplierInvoiceReceivedHTTotal: number,
    public supplierInvoiceReceivedTTCTotal: number,
    public marginTotal: number,
  ) {
  }
}
