import {IMonthlySummaryDataTableData} from "./monthly-summary-data-table-data.model";

export interface IDataTableResponse {
  data: IMonthlySummaryDataTableData[];
  draw: number;
  recordsFiltered: number;
  recordsTotal: number;
}

export class DataTableResponse implements IDataTableResponse {
  constructor(
    public data: IMonthlySummaryDataTableData[],
    public draw: number,
    public recordsFiltered: number,
    public recordsTotal: number
  ) {
  }
}
