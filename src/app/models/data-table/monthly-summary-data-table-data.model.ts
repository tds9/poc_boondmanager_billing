import {TimesReportValue} from "../api/times-report-value.model";

export interface IMonthlySummaryDataTableData {
  hasInvoiceForThisMonth?: boolean;
  invoiceIdForThisMonth?: number;
  hasOrderForThisMonth?: boolean;
  orderIdForThisMonth?: number;
  purchaseID?: number;
  timesReportStateForThisMonth?: TimesReportValue | number;
  hasTimesReportForThisMonth?: boolean;
  timesReportIdForThisMonth?: number;
  projectIDForThisMonth?: number;
  benifitIdForThisMonth?: number;
  hasBenifits?: boolean;
  hasBenifitsForThisMonth?: boolean;
  consultantID?: number;
  consultantFullName?: string;
  finalCustomerID?: number;
  finalCustomerName?: string;
  isEmployee?: boolean;
  commercialID?: number;
  commercialFullName?: string;
  monthProduction?: number;
  numberDaysOrdered?: number;
  orderRemainingDays?: number;
  cjm?: number;
  tjm?: number;
  forecastCustomerBillingHT?: number;
  provisionalInvoicingSupplierHT?: number;
  margin?: number;
  supplierInvoiceReceivedHT?: number;
  supplierInvoiceReceivedTTC?: number;
  supplierPayment?: string;
  customerInvoiceReceivedHT?: number;
  customerInvoiceReceivedTTC?: number;
  customerPayment?: string;
}

export class MonthlySummaryDataTableData implements IMonthlySummaryDataTableData {
  constructor(
    public purchaseID?: number,
    public timesReportStateForThisMonth?: TimesReportValue,
    public hasTimesReportForThisMonth?: boolean,
    public timesReportIdForThisMonth?: number,
    public benifitIdForThisMonth?: number,
    public hasBenifits?: boolean,
    public hasBenifitsForThisMonth?: boolean,
    public consultantID?: number,
    public consultantFullName?: string,
    public finalCustomerID?: number,
    public finalCustomerName?: string,
    public isEmployee?: boolean,
    public commercialID?: number,
    public commercialFullName?: string,
    public monthProduction?: number,
    public numberDaysOrdered?: number,
    public orderRemainingDays?: number,
    public cjm?: number,
    public tjm?: number,
    public forecastCustomerBillingHT?: number,
    public provisionalInvoicingSupplierHT?: number,
    public margin?: number,
    public supplierInvoiceReceivedHT?: number,
    public supplierInvoiceReceivedTTC?: number,
    public supplierPayment?: string,
  ) {
  }
}
